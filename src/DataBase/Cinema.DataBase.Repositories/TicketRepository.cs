using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class TicketRepository : ITicketRepository
{
    private readonly CinemaContext _dbContext;

    public TicketRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddTicketRepositories(Guid id, int price, bool sold, Guid sessionId, Guid seatId,
        Guid? userId = null)
    {
        await _dbContext.Tickets.AddAsync(new Models.Ticket(id,
            price,
            sold,
            seatId,
            sessionId,
            userId));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Ticket>> GetTicketsAsync()
    {
        var dbTickets = await _dbContext.Tickets.ToListAsync();

        return dbTickets.Select(TicketConverter.ConvertTicketToCore).ToList()!;
    }

    public async Task<Ticket?> FindTicketAsync(Guid id)
    {
        var dbTicket = await _dbContext.Tickets.FindAsync(id);

        return TicketConverter.ConvertTicketToCore(dbTicket);
    }

    public async Task<Ticket?> FindTicketBySeatAndSessionIdAsync(Guid seatId, Guid sessionId)
    {
        var dbTicket = await _dbContext
            .Tickets
            .FirstOrDefaultAsync(p => p.SeatId == seatId && p.SessionId == sessionId);

        return TicketConverter.ConvertTicketToCore(dbTicket);
    }

    public async Task DeleteTicketAsync(Guid id)
    {
        var dbTicket = await _dbContext.Tickets.FindAsync(id);
        
        if(dbTicket is null)
            return;

        _dbContext.Tickets.Remove(dbTicket);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateTicketAsync(Guid id, int price, bool sold, Guid sessionId, Guid seatId,
        Guid? userId = null)
    {
        var ticket = await _dbContext.Tickets.FindAsync(id);
        
        if(ticket is null)
            return;

        ticket.Price = price;
        ticket.Sold = sold;
        ticket.UserId = userId;
        ticket.SessionId = sessionId;
        ticket.SeatId = seatId;
        
        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Tickets.AnyAsync(p => p.Id == id);
    }
}