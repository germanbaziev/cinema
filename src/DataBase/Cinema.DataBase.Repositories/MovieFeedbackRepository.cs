using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Models;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class MovieFeedbackRepository : IMovieFeedbackRepository
{
    private readonly CinemaContext _dbContext;

    public MovieFeedbackRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddMovieFeedBackAsync(Guid id, string text, DateTimeOffset date, bool liked, Guid movieId, Guid userId)
    {
        await _dbContext.MovieFeedbacks.AddAsync(new MovieFeedback(id,
            text,
            date,
            liked,
            movieId,
            userId));

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateMovieFeedbackAsync(Guid id, string text, DateTimeOffset date, bool liked, Guid movieId, Guid userId)
    {
        var movieFeedback = await _dbContext.MovieFeedbacks.FindAsync(id);
        
        if(movieFeedback is null)
            return;

        movieFeedback.Text = text;
        movieFeedback.Date = date;
        movieFeedback.Liked = liked;
        movieFeedback.MovieId = movieId;
        movieFeedback.UserId = userId;

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Core.Models.MovieFeedback>> GetMovieFeedbacksAsync()
    {
        var movieFeedBacks = await _dbContext.MovieFeedbacks.ToListAsync();

        return movieFeedBacks.Select(MovieFeedbackConverter.ConvertMovieFeedbackToCore).ToList()!;
    }

    public async Task<Core.Models.MovieFeedback?> GetMovieFeedbackAsync(Guid id)
    {
        var movieFeedback = await _dbContext.MovieFeedbacks.FindAsync(id);

        return MovieFeedbackConverter.ConvertMovieFeedbackToCore(movieFeedback);
    }

    public Task<bool> ExistsAsync(Guid id)
    {
        return _dbContext.MovieFeedbacks.AnyAsync(p => p.Id == id);
    }

    public async Task RemoveMovieFeedback(Guid id)
    {
        var movieFeedback = await _dbContext.MovieFeedbacks.FindAsync(id);

        if(movieFeedback is null)
            throw new InvalidOperationException();
        
        _dbContext.MovieFeedbacks.Remove(movieFeedback);

        await _dbContext.SaveChangesAsync();
    }
}