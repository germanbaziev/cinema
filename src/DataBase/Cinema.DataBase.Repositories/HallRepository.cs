using System.Formats.Asn1;
using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class HallRepository : IHallRepository
{
    private readonly CinemaContext _dbContext;

    public HallRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddHallAsync(Guid id,
        int number,
        string type,
        Guid cinemaId)
    {
        await _dbContext.Halls.AddAsync(new Models.Hall(id,
            number,
            type,
            cinemaId));
    
        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Hall>> GetHallsAsync()
    {
        var dbHalls = await _dbContext.Halls.ToListAsync();

        return dbHalls.Select(HallConverter.ConvertHallToCore).ToList()!;
    }

    public async Task<Hall?> FindHallAsync(Guid id)
    {
        var dbHall = await _dbContext.Halls
            .Include(p => p.Seats)
            .FirstOrDefaultAsync(p => p.Id == id);
        
        return HallConverter.ConvertHallToCore(dbHall);
    }

    public async Task DeleteHallAsync(Guid id)
    {
        var dbHall = await _dbContext.Halls.FindAsync(id);
        
        if(dbHall is null)
            return;

        _dbContext.Halls.Remove(dbHall);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateHallAsync(Guid id,
        int number,
        string type,
        Guid cinemaId)
    {
        var hall = await _dbContext.Halls.FindAsync(id);

        if (hall is null)
            return;

        hall.Number = number;
        hall.Type = type;
        hall.CinemaId = cinemaId;

        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Halls.AnyAsync(p => p.Id == id);
    }
}