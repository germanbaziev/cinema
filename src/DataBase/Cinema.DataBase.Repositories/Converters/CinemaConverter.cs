using Cinema.Core.Models;
using DbCinema = Cinema.DataBase.Models.Cinema;
using CoreCinema = Cinema.Core.Models.Cinema;
using CoreMovieCinema = Cinema.Core.Models.MovieCinema;

namespace Cinema.DataBase.Repositories.Converters;

public static class CinemaConverter
{
    public static CoreCinema? ConvertCinemaToCore(DbCinema? dbCinema)
    {
        if (dbCinema is null)
            return null;

        var coreMovieCinemas = dbCinema.MovieCinemas is null
            ? new List<CoreMovieCinema>()
            : dbCinema.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToCore).ToList()!;

        var coreHalls = dbCinema.Halls is null
            ? new List<Hall>()
            : dbCinema.Halls.Select(HallConverter.ConvertHallToCore).ToList()!;
        
        return new CoreCinema(dbCinema.Id,
            dbCinema.Description,
            dbCinema.AvatarId,
            dbCinema.Address,
            dbCinema.Name,
            coreMovieCinemas,
            coreHalls);
    }
    
    public static DbCinema? ConvertCinemaToDb(CoreCinema? coreCinema)
    {
        if (coreCinema is null)
            return null;
        
        return new DbCinema(coreCinema.Id,
            coreCinema.Description,
            coreCinema.AvatarId,
            coreCinema.Address,
            coreCinema.Name);
    }
}