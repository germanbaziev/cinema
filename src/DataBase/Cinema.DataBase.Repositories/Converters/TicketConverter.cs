using DbTicket = Cinema.DataBase.Models.Ticket;
using CoreTicket = Cinema.Core.Models.Ticket;

namespace Cinema.DataBase.Repositories.Converters;

public static class TicketConverter
{
    public static CoreTicket? ConvertTicketToCore(DbTicket? dbTicket)
    {
        if (dbTicket is null)
            return null;
        
        return new CoreTicket(dbTicket.Id,
            dbTicket.Price,
            dbTicket.Sold,
            dbTicket.UserId,
            dbTicket.SessionId,
            dbTicket.SeatId);
    }

    public static DbTicket? ConvertTicketToDb(CoreTicket? coreTicket)
    {
        if (coreTicket is null)
            return null;
 
        return new DbTicket(coreTicket.Id,
            coreTicket.Price,
            coreTicket.Sold,
            coreTicket.SeatId,
            coreTicket.SessionId,
            coreTicket.UserId);
    }
}