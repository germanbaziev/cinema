using Cinema.Core.Models;
using CoreSession = Cinema.Core.Models.Session;
using DbSession = Cinema.DataBase.Models.Session;

namespace Cinema.DataBase.Repositories.Converters;

public static class SessionConverter
{
    public static CoreSession? ConvertSessionToCore(DbSession? dbSession)
    {
        if (dbSession is null)
            return null;

        var coreTickets = dbSession.Tickets is null
            ? new List<Ticket>()
            : dbSession.Tickets.Select(TicketConverter.ConvertTicketToCore).ToList()!;
        
        return new CoreSession(dbSession.Id,
            dbSession.StartDate,
            dbSession.MovieId,
            dbSession.HallId,
            coreTickets,
            dbSession.CinemaId);
    }

    public static DbSession? ConvertSessionToDb(CoreSession? coreSession)
    {
        if (coreSession is null)
            return null;

        return new DbSession(coreSession.Id,
            coreSession.StartDate,
            coreSession.HallId,
            coreSession.MovieId,
            coreSession.CinemaId);
    }
}