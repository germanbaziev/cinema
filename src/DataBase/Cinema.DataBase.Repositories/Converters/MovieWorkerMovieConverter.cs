using Cinema.DataBase.Models;

namespace Cinema.DataBase.Repositories.Converters;

public static class MovieWorkerMovieConverter
{
    public static Core.Models.MovieWorkerMovie? ConvertMovieWorkerMovieToCore(MovieWorkerMovie? movieWorkerMovie)
    {
        if (movieWorkerMovie is null)
            return null;
        
        return new Core.Models.MovieWorkerMovie(movieWorkerMovie.Id,
            movieWorkerMovie.MovieWorkerId,
            movieWorkerMovie.MovieId);
    }

    public static MovieWorkerMovie? ConvertToDbMovieWorkerMovie(Core.Models.MovieWorkerMovie? coreMovieWorkerMovie)
    {
        if (coreMovieWorkerMovie is null)
            return null;
        
        return new MovieWorkerMovie(coreMovieWorkerMovie.Id,
            coreMovieWorkerMovie.MovieId,
            coreMovieWorkerMovie.MovieWorkerId);
    }
}