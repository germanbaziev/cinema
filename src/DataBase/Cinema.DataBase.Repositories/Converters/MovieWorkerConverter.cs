using Cinema.Core.Models;
using CoreMovieWorker = Cinema.Core.Models.MovieWorker;
using DBMovieWorker = Cinema.DataBase.Models.MovieWorker;

namespace Cinema.DataBase.Repositories.Converters;

public static class MovieWorkerConverter
{
    public static CoreMovieWorker? ConvertToCoreMovieWorker(DBMovieWorker? dbMovieWorker)
    {
        if (dbMovieWorker is null)
            return null;
        
        var coreMovieWorkerMovies = dbMovieWorker.MovieWorkerMovies is null
            ? new List<MovieWorkerMovie>() 
            : dbMovieWorker.MovieWorkerMovies.Select(MovieWorkerMovieConverter.ConvertMovieWorkerMovieToCore).ToList()!;

        return new CoreMovieWorker(dbMovieWorker.Id,
            dbMovieWorker.AvatarId,
            dbMovieWorker.Name,
            dbMovieWorker.Role,
            coreMovieWorkerMovies);
    }
    
    public static DBMovieWorker? ConvertToDbMovieWorker(CoreMovieWorker? coreMovieWorker)
    {
        if (coreMovieWorker is null)
            return null;
                
        return new DBMovieWorker(coreMovieWorker.Id,
            coreMovieWorker.AvatarId,
            coreMovieWorker.Name,
            coreMovieWorker.Role);
    }
}