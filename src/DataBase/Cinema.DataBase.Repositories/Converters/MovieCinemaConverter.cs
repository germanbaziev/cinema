using DbMovieCinema = Cinema.DataBase.Models.MovieCinema;
using CoreMovieCinema = Cinema.Core.Models.MovieCinema;
namespace Cinema.DataBase.Repositories.Converters;

public static class MovieCinemaConverter
{
    public static CoreMovieCinema? ConvertMovieCinemaToCore (DbMovieCinema? dbMovieCinema)
    {
        if (dbMovieCinema is null)
            return null;
        
        return new CoreMovieCinema(dbMovieCinema.Id,
            dbMovieCinema.CinemaId,
            dbMovieCinema.MovieId);
    }
    
    public static DbMovieCinema? ConvertMovieCinemaToDb (CoreMovieCinema? coreMovieCinema)
    {
        if (coreMovieCinema is null)
            return null;
        
        return new DbMovieCinema(coreMovieCinema.Id,
            coreMovieCinema.CinemaId,
            coreMovieCinema.MovieId);
    }
}