using Cinema.Core.Models;
using DbHall = Cinema.DataBase.Models.Hall;
using CoreHall = Cinema.Core.Models.Hall;

namespace Cinema.DataBase.Repositories.Converters;

public static class HallConverter
{
    public static CoreHall? ConvertHallToCore(DbHall? dbHall)
    {
        if (dbHall is null)
            return null;

        var coreSeat = dbHall.Seats is null
            ? new List<Seat>()
            : dbHall.Seats.Select(SeatConverter.ConvertSeatToCore).ToList()!;
        
        return new CoreHall(dbHall.Id,
            dbHall.Number,
            dbHall.Type,
            dbHall.CinemaId,
            coreSeat);
    }
    
    public static DbHall? ConvertHallToDb(CoreHall? coreHall)
    {
        if (coreHall is null)
            return null;

        return new DbHall(coreHall.Id,
            coreHall.Number,
            coreHall.Type,
            coreHall.CinemaId);
    }
}