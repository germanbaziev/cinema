using Cinema.Core.Models;
using DbMovie = Cinema.DataBase.Models.Movie;
using CoreMovie = Cinema.Core.Models.Movie;
using MovieFeedback = Cinema.DataBase.Models.MovieFeedback;
using Session = Cinema.DataBase.Models.Session;

namespace Cinema.DataBase.Repositories.Converters;

public static class MovieConverter
{
    public static CoreMovie? ConvertMovieToCore(DbMovie? dbMovie)
    {
        if (dbMovie is null)
            return null;

        var coreSessions = dbMovie.Sessions is null
            ? new List<Core.Models.Session>()
            : dbMovie.Sessions.Select(SessionConverter.ConvertSessionToCore).ToList()!;
        
        var coreMovieFeedbacks = dbMovie.MovieFeedbacks is null
            ? new List<Core.Models.MovieFeedback>()
            : dbMovie.MovieFeedbacks.Select(MovieFeedbackConverter.ConvertMovieFeedbackToCore).ToList()!;

        var coreMovieWorkerMovie = dbMovie.MovieWorkerMovies is null
            ? new List<Core.Models.MovieWorkerMovie>()
            : dbMovie.MovieWorkerMovies.Select(MovieWorkerMovieConverter.ConvertMovieWorkerMovieToCore).ToList()!;

        var coreMovieCinemas = dbMovie.MovieCinemas is null
            ? new List<MovieCinema>()
            : dbMovie.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToCore).ToList()!;
        
        return new CoreMovie(dbMovie.Id,
            dbMovie.TrailerId,
            dbMovie.AvatarId,
            dbMovie.Name,
            dbMovie.Description,
            dbMovie.Country,
            dbMovie.AgeLimit,
            dbMovie.Director,
            coreSessions,
            coreMovieWorkerMovie,
            coreMovieFeedbacks,
            coreMovieCinemas);
    }

    public static DbMovie? ConvertMovieToDb(CoreMovie? coreMovie)
    {
        if (coreMovie is null)
            return null;

        return new DbMovie(coreMovie.Id,
            coreMovie.TrailerId,
            coreMovie.AvatarId,
            coreMovie.Name,
            coreMovie.Description,
            coreMovie.Country,
            coreMovie.AgeLimit,
            coreMovie.Director);
    }
}