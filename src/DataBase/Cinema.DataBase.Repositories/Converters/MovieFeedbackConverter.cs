using DbMovieFeedback = Cinema.DataBase.Models.MovieFeedback;
using CoreMovieFeedback = Cinema.Core.Models.MovieFeedback;
namespace Cinema.DataBase.Repositories.Converters;

public static class MovieFeedbackConverter
{
    public static CoreMovieFeedback? ConvertMovieFeedbackToCore(DbMovieFeedback? dbMovieFeedback)
    {
        if (dbMovieFeedback is null)
            return null;

        return new CoreMovieFeedback(dbMovieFeedback.Id,
            dbMovieFeedback.Text,
            dbMovieFeedback.Date,
            dbMovieFeedback.Liked,
            dbMovieFeedback.MovieId,
            dbMovieFeedback.UserId);
    }
    
    public static DbMovieFeedback? ConvertMovieFeedbackToDb(CoreMovieFeedback? coreMovieFeedback)
    {
        if (coreMovieFeedback is null)
            return null;

        return new DbMovieFeedback(coreMovieFeedback.Id,
            coreMovieFeedback.Text,
            coreMovieFeedback.Date,
            coreMovieFeedback.Liked,
            coreMovieFeedback.MovieId,
            coreMovieFeedback.UserId);
    }
}