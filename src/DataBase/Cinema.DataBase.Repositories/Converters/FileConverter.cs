using CoreFile = Cinema.Core.Models.File;
using DbFile = Cinema.DataBase.Models.File;

namespace Cinema.DataBase.Repositories.Converters;

public static class FileConverter
{
    public static CoreFile? ConvertFileToCore(DbFile? dbFile)
    {
        if (dbFile is null)
            return null;

        return new CoreFile(dbFile.Id,
            dbFile.Data,
            dbFile.Name);
    }

    public static DbFile? ConvertFileToDb(CoreFile? coreFile)
    {
        if (coreFile is null)
            return null;

        return new DbFile(coreFile.Id,
            coreFile.Data,
            coreFile.Name);
    }
}