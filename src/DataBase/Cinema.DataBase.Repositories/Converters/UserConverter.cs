using Cinema.Core.Models;
using CoreUser = Cinema.Core.Models.User;
using DbUser = Cinema.DataBase.Models.User;

namespace Cinema.DataBase.Repositories.Converters;

public static class UserConverter
{
    public static CoreUser? ConvertUserToCore(DbUser? dbUser)
    {
        if (dbUser is null)
            return null;

        var coreTickets = dbUser.Tickets is null
            ? new List<Ticket>()
            : dbUser.Tickets.Select(TicketConverter.ConvertTicketToCore).ToList()!;
        
        return new CoreUser(dbUser.Id,
            dbUser.AvatarId,
            dbUser.Password,
            dbUser.Login,
            dbUser.Telegram,
            dbUser.Phone,
            dbUser.Email,
            dbUser.Permissions,
            coreTickets);
    }

    public static DbUser? ConvertUserToDb(CoreUser? coreUser)
    {
        if (coreUser is null)
            return null;

        return new DbUser(coreUser.Id,
            coreUser.AvatarId,
            coreUser.Password,
            coreUser.Login,
            coreUser.Telegram,
            coreUser.Phone,
            coreUser.Email,
            coreUser.Permissions);
    }
}