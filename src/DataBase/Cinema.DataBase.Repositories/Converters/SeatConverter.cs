using Cinema.Core.Models;
using DbSeat = Cinema.DataBase.Models.Seat;
using CoreSeat = Cinema.Core.Models.Seat;

namespace Cinema.DataBase.Repositories.Converters;

public static class SeatConverter
{
    public static CoreSeat? ConvertSeatToCore(DbSeat? dbSeat)
    {
        if (dbSeat is null)
            return null;

        return new CoreSeat(dbSeat.Id,
            dbSeat.Type,
            dbSeat.Row,
            dbSeat.Number,
            dbSeat.HallId);
    }

    public static DbSeat? ConvertSeatToDb(CoreSeat? coreSeat)
    {
        if (coreSeat is null)
            return null;

        return new DbSeat(coreSeat.Id,
            coreSeat.Type,
            coreSeat.Row,
            coreSeat.Number,
            coreSeat.HallId);
    }
}