using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Models;
using Cinema.DataBase.Repositories.Converters;
using File = Cinema.Core.Models.File;

namespace Cinema.DataBase.Repositories;

public class FileRepository : IFileRepository
{
    private readonly CinemaContext _dbContext;

    public FileRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddFileAsync(Guid id, string name, byte[]? data = null)
    {
        await _dbContext.Files.AddAsync(new Models.File(id,
            data,
            name));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<File?> FindFileAsync(Guid id)
    {
        var file = await _dbContext.Files.FindAsync(id);

        return FileConverter.ConvertFileToCore(file);
    }
}