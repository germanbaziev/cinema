using Cinema.Core.Repositories;
using Cinema.DataBase.Context;

namespace Cinema.DataBase.Repositories;

public class MovieCinemaRepository : IMovieCinemaRepository
{
    private readonly CinemaContext _dbContext;

    public MovieCinemaRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddMovieCinemaAsync(Guid id, Guid cinemaId, Guid movieId)
    {
        await _dbContext.MovieCinemas.AddAsync(new Models.MovieCinema(id,
            cinemaId,
            movieId));

        await _dbContext.SaveChangesAsync();
    }
}