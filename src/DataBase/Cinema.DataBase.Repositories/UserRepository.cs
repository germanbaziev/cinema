using System.Diagnostics;
using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class UserRepository : IUserRepository
{
    private readonly CinemaContext _dbContext;

    public UserRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddUserAsync(Guid id, string password, string login, string permissions,
        string? telegram = null, string? phone = null, string? email = null, Guid? avatarId = null)
    {
        await _dbContext.Users.AddAsync(new Models.User(id,
            avatarId,
            password,
            login,
            telegram,
            phone,
            email,
            permissions));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<bool> AuthUser(string name, string password)
    {
        return await _dbContext.Users.AnyAsync(p => p.Login == name && p.Password == password);
    }

    public async Task<User?> GetUserData(string name, string password)
    {
        var user = await _dbContext.Users.FirstOrDefaultAsync(p => p.Login == name && p.Password == password);

        return UserConverter.ConvertUserToCore(user);
    }

    public async Task<List<User>> GetUsersAsync()
    {
        var dbUsers = await _dbContext.Users.ToListAsync();

        return dbUsers.Select(UserConverter.ConvertUserToCore).ToList()!;
    }

    public async Task<User?> FindUserAsync(Guid id)
    {
        var dbUser = await _dbContext.Users
            .Include(p => p.Tickets)
            .FirstOrDefaultAsync(p => p.Id == id);

        return UserConverter.ConvertUserToCore(dbUser);
    }

    public async Task DeleteUserAsync(Guid id)
    {
        var dbUser = await _dbContext.Users.FindAsync(id);

        if (dbUser is null)
            return;

        _dbContext.Users.Remove(dbUser);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateUserAsync(Guid id, string password, string login, string permissions, string? telegram = null,
        string? phone = null, string? email = null, Guid? avatarId = null)
    {
        var user = await _dbContext.Users.FindAsync(id);
        
        if(user is null)
            return;

        user.Password = password;
        user.Login = login;
        user.Permissions = permissions;
        user.Telegram = telegram;
        user.Phone = phone;
        user.Email = email;
        user.AvatarId = avatarId;

        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Users.AnyAsync(p => p.Id == id);
    }
}