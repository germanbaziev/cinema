using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class MovieRepository : IMovieRepository
{
    private readonly CinemaContext _dbContext;

    public MovieRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddMovieRepositories(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director)
    {
        await _dbContext.Movies.AddAsync(new Models.Movie(id,
            trailerId,
            avatarId,
            name,
            description,
            country,
            ageLimit,
            director));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Movie>> GetMoviesAsync()
    {
        var dbMovie = await _dbContext.Movies.ToListAsync();

        return dbMovie.Select(MovieConverter.ConvertMovieToCore).ToList()!;
    }

    public async Task<Movie?> FindMovieAsync(Guid id)
    {
        var dbMovie = await _dbContext.Movies
            .Include(p => p.MovieCinemas)
            .Include(p => p.MovieWorkerMovies)
            .Include(p => p.MovieFeedbacks)
            .FirstOrDefaultAsync(p => p.Id == id);
        
        return MovieConverter.ConvertMovieToCore(dbMovie);
    }

    public async Task DeleteMovieAsync(Guid id)
    {
        var dbMovie = await _dbContext.Movies.FindAsync(id);
        
        if(dbMovie is null)
            return;

        _dbContext.Movies.Remove(dbMovie);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<Movie?> FindMovieByName(string movieName)
    {
        var dbMovie = await _dbContext.Movies
            .Include(p => p.MovieCinemas)
            .Include(p => p.MovieWorkerMovies)
            .Include(p => p.MovieFeedbacks)
            .FirstOrDefaultAsync(p => p.Name == movieName);

        return MovieConverter.ConvertMovieToCore(dbMovie);
    }
    
    public async Task UpdateMovieAsync(Guid id, Guid trailerId, Guid avatarId, string name, string description, string country,
        int ageLimit, string director)
    {
        var movie = await _dbContext.Movies.FindAsync(id);
        
        if(movie is null)
            return;

        movie.TrailerId = trailerId;
        movie.AvatarId = avatarId;
        movie.Name = name;
        movie.Description = description;
        movie.Country = country;
        movie.AgeLimit = ageLimit;
        movie.Director = director;

        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Movies.AnyAsync(p => p.Id == id);
    }
}