using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Models;

namespace Cinema.DataBase.Repositories;

public class MovieWorkerMovieRepository : IMovieWorkerMovieRepository
{
    private readonly CinemaContext _dbContext;

    public MovieWorkerMovieRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddMovieWorkerMovieAsync(Guid id, Guid movieWorkerId, Guid movieId)
    {
        await _dbContext.MovieWorkerMovies.AddAsync(new MovieWorkerMovie(id, movieId, movieWorkerId));

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateMovieWorkerMovieAsync(Guid id, Guid movieWorkerId, Guid movieId)
    {
        var movieWorkerMovie = await _dbContext.MovieWorkerMovies.FindAsync(id);
        
        if(movieWorkerMovie is null)
            return;

        movieWorkerMovie.MovieWorkerId = movieWorkerId;
        movieWorkerMovie.MovieId = movieId;

        await _dbContext.SaveChangesAsync();
    }
}