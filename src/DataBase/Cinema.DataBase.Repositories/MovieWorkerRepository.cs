using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Cinema.DataBase.Repositories.Converters;

namespace Cinema.DataBase.Repositories;

public class MovieWorkerRepository : IMovieWorkerRepository
{
    private readonly CinemaContext _dbContext;

    public MovieWorkerRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddMovieWorkerAsync(Guid id,
        Guid avatarId,
        string name,
        string role)
    {
        await _dbContext.MovieWorker.AddAsync(new Models.MovieWorker(id,
            avatarId,
            name,
            role));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<MovieWorker>> GetMovieWorkersAsync()
    {
        var dbMovieWorkers = await _dbContext.MovieWorker.ToListAsync();

        return dbMovieWorkers.Select(MovieWorkerConverter.ConvertToCoreMovieWorker).ToList()!;
    }

    public async Task<MovieWorker?> FindMovieWorkerAsync(Guid id)
    {
        var dbMovieWorker = await _dbContext.MovieWorker.FindAsync(id);

        return MovieWorkerConverter.ConvertToCoreMovieWorker(dbMovieWorker);
    }

    public async Task DeleteMovieWorkerAsync(Guid id)
    {
        var movieWorker = await _dbContext.MovieWorker.FindAsync(id);

        if(movieWorker is null)
            return;
        
        _dbContext.MovieWorker.Remove(movieWorker);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateMovieWorkerAsync(Guid id, Guid avatarId, string name, string role)
    {
        var movieWorker = await _dbContext.MovieWorker.FindAsync(id);
        
        if(movieWorker is null)
            return;
        
        movieWorker.AvatarId = avatarId;
        movieWorker.Name = name;
        movieWorker.Role = role;

        _dbContext.MovieWorker.Update(movieWorker);

        await _dbContext.SaveChangesAsync();
    }

    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.MovieWorker.AnyAsync(p => p.Id == id);
    }
}