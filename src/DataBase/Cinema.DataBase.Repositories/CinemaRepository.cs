using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;
using DbCinema = Cinema.DataBase.Models.Cinema;
using MovieCinema = Cinema.Core.Models.MovieCinema;

namespace Cinema.DataBase.Repositories;

public class CinemaRepository : ICinemaRepository
{
    private readonly CinemaContext _dbContext;

    public CinemaRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task AddCinemaAsync(Guid id,
        string description,
        Guid avatarId,
        string address,
        string name)
    {
        await _dbContext.Cinemas.AddAsync(new DbCinema(id,
            description,
            avatarId,
            address,
            name));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Core.Models.Cinema>> GetCinemasAsync()
    {
        var dbCinemas = await _dbContext.Cinemas.ToListAsync();

        return dbCinemas.Select(CinemaConverter.ConvertCinemaToCore).ToList()!;
    }

    public async Task<Core.Models.Cinema?> FindCinemaAsync(Guid id)
    {
        var cinema = await _dbContext.Cinemas.Include(p => p.MovieCinemas).FirstOrDefaultAsync(p => p.Id == id);

        return CinemaConverter.ConvertCinemaToCore(cinema);
    }

    public async Task DeleteCinemaAsync(Guid id)
    {
        var cinema = await _dbContext.Cinemas.FindAsync(id);

        if(cinema is null)
            return;
        
        _dbContext.Cinemas.Remove(cinema);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<Core.Models.Cinema?> FindCinemaByNameAsync(string cinemaName)
    {
        var cinema = await _dbContext.Cinemas.Include(p => p.MovieCinemas).FirstOrDefaultAsync(p => p.Name == cinemaName);
        
        return CinemaConverter.ConvertCinemaToCore(cinema);
    }

    public async Task UpdateCinemaAsync(Guid id, string description, Guid avatarId, string address, string name)
    {
        var cinema = await _dbContext.Cinemas.FindAsync(id);
        
        if(cinema is null)
            return;

        cinema.Description = description;
        cinema.AvatarId = avatarId;
        cinema.Address = address;
        cinema.Name = name;

        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Cinemas.AnyAsync(p => p.Id == id);
    }
}