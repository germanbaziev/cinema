using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class AuthRepository : IAuthRepository
{
    private readonly CinemaContext _dbContext;

    public AuthRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<User?> GetUserAsync(string login, string password)
    {
        var dbUser = await _dbContext.Users
            .FirstOrDefaultAsync(p => p.Login == login && p.Password == password);

        return UserConverter.ConvertUserToCore(dbUser);
    }

    public Task<bool> ExistUserAsync(string login, string password)
    {
        return _dbContext.Users.AnyAsync(p => p.Login == login && p.Password == password);
    }
}