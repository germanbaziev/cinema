using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class SessionRepository : ISessionRepository
{
    private readonly CinemaContext _dbContext;

    public SessionRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddSessionAsync(Guid id, DateTimeOffset startDate, Guid movieId, Guid hallId, Guid cinemaId)
    {
        await _dbContext.Sessions.AddAsync(new Models.Session(id,
            startDate,
            hallId,
            movieId,
            cinemaId));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Session>> GetSessionsByCinemaAndMovieIdAsync(Guid cinemaId, Guid movieId)
    {
        var dbSessions = await _dbContext.Sessions.Where(p => p.CinemaId == cinemaId && p.MovieId == movieId).ToListAsync();

        return dbSessions.Select(SessionConverter.ConvertSessionToCore).ToList()!;
    }

    public async Task<List<Session>> GetSessionsAsync()
    {
        var dbSessions = await _dbContext.Sessions.ToListAsync();

        return dbSessions.Select(SessionConverter.ConvertSessionToCore).ToList()!;
    }
    public async Task<Session?> FindSessionAsync(Guid id)
    {
        var dbSession = await _dbContext.Sessions.FindAsync(id);

        return SessionConverter.ConvertSessionToCore(dbSession);
    }

    public async Task DeleteSessionAsync(Guid id)
    {
        var dbSession = await _dbContext.Sessions.FindAsync(id);
        
        if(dbSession is null)
            throw new InvalidOperationException();

        _dbContext.Sessions.Remove(dbSession);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateSessionAsync(Guid id, DateTimeOffset startDate, Guid movieId, Guid hallId, Guid cinemaId)
    {
        var session = await _dbContext.Sessions.FindAsync(id);

        if (session is null)
            throw new InvalidOperationException();

        session.StartDate = startDate;
        session.MovieId = movieId;
        session.HallId = hallId;
        session.CinemaId = cinemaId;
        
        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Sessions.AnyAsync(p => p.Id == id);
    }
}