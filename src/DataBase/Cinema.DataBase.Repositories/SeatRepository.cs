using Cinema.Core.Models;
using Cinema.Core.Repositories;
using Cinema.DataBase.Context;
using Cinema.DataBase.Repositories.Converters;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DataBase.Repositories;

public class SeatRepository : ISeatRepository
{
    private readonly CinemaContext _dbContext;

    public SeatRepository(CinemaContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task AddSeatAsync(Guid id, string type, int row, int number, Guid hallId)
    {
        await _dbContext.Seats.AddAsync(new Models.Seat(id,
            type,
            row,
            number,
            hallId));

        await _dbContext.SaveChangesAsync();
    }

    public async Task<List<Seat>> GetSeatsAsync()
    {
        var dbSeats = await _dbContext.Seats.ToListAsync();

        return dbSeats.Select(SeatConverter.ConvertSeatToCore).ToList()!;
    }

    public async Task<Seat?> FindSeatAsync(Guid id)
    {
        var dbSeat = await _dbContext.Seats.FindAsync(id);

        return SeatConverter.ConvertSeatToCore(dbSeat);
    }

    public async Task DeleteSeatAsync(Guid id)
    {
        var seat = await _dbContext.Seats.FindAsync(id);
        
        if(seat is null)
            return;

        _dbContext.Seats.Remove(seat);

        await _dbContext.SaveChangesAsync();
    }

    public async Task UpdateSeatAsync(Guid id, string type, int row, int number, Guid hallId)
    {
        var seat = await _dbContext.Seats.FindAsync(id);
        
        if(seat is null)
            return;

        seat.Type = type;
        seat.Row = row;
        seat.Number = number;
        seat.HallId = hallId;

        await _dbContext.SaveChangesAsync();
    }
    
    public Task<bool> ExistAsync(Guid id)
    {
        return _dbContext.Seats.AnyAsync(p => p.Id == id);
    }
}