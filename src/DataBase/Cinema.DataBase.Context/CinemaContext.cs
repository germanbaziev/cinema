﻿using System.Net;
using Microsoft.EntityFrameworkCore;
using Cinema.DataBase.Models;

namespace Cinema.DataBase.Context

#nullable disable

{
    public class CinemaContext : DbContext
    {
        public DbSet<MovieWorker> MovieWorker { get; set; }

        public DbSet<MovieWorkerMovie> MovieWorkerMovies { get; set; }

        public DbSet<Models.Cinema> Cinemas { get; set; }

        public DbSet<Models.File> Files { get; set; }

        public DbSet<Hall> Halls { get; set; }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<MovieCinema> MovieCinemas { get; set; }

        public DbSet<MovieFeedback> MovieFeedbacks { get; set; }

        public DbSet<Seat> Seats { get; set; }

        public DbSet<Session> Sessions { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<User> Users { get; set; }

        public CinemaContext(DbContextOptions<CinemaContext> options)
            : base(options)
        {
        }

        public CinemaContext()
        {
        }
    }
    
#nullable restore
    
}