using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class Movie
    {
        public Guid Id { get; set; }
        
        public Guid TrailerId { get; set; }
        
        public Guid AvatarId { get; set; }
        
        public string Name { get; set; }

        public string Description { get; set; }
        
        public string Country { get; set; }
        
        public int AgeLimit { get; set; }
        
        public string Director { get; set; }

        public Movie(Guid id,
            Guid trailerId,
            Guid avatarId,
            string name,
            string description,
            string country,
            int ageLimit,
            string director)
        {
            Id = id;
            TrailerId = trailerId;
            AvatarId = avatarId;
            Name = name;
            Description = description;
            Country = country;
            AgeLimit = ageLimit;
            Director = director;
        }
        
        public ICollection<Session>? Sessions { get; set; }
        
        public ICollection<MovieWorkerMovie>? MovieWorkerMovies { get; set; }
        
        public ICollection<MovieFeedback>? MovieFeedbacks { get; set; }
        
        public ICollection<MovieCinema>? MovieCinemas { get; set; }

    }
}