using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class User
    {
        public Guid Id { get; set; }
        
        public Guid? AvatarId { get; set; }
        
        public string Password { get; set; }
        
        public string Login { get; set; }
        
        public string? Telegram { get; set; }
        
        public string? Phone { get; set; }
        
        public string? Email { get; set; }
        
        public string Permissions { get; set; }
        
        public User(Guid id,
            Guid? avatarId,
            string password,
            string login,
            string? telegram,
            string? phone,
            string? email,
            string permissions)
        {
            Id = id;
            AvatarId = avatarId;
            Password = password;
            Login = login;
            Telegram = telegram;
            Phone = phone;
            Email = email;
            Permissions = permissions;
        }
        
        public ICollection<Ticket>? Tickets { get; set; }
        
        public ICollection<MovieFeedback>? MovieFeedbacks { get; set; }
    }
}