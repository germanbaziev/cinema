using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class Seat
    {
        public Guid Id { get; set; }
        
        public string Type { get; set; }
        
        public int Row { get; set; }
        
        public int Number { get; set; }
        
        public Guid HallId { get; set; }
        
        public Seat(Guid id,
            string type,
            int row,
            int number,
            Guid hallId)
        {
            Id = id;
            Type = type;
            Row = row;
            Number = number;
            HallId = hallId;
        }
        
        public Hall? Hall { get; set; }
        
        public ICollection<Ticket>? Tickets { get; set; }
    }
}