﻿
namespace Cinema.DataBase.Models
{
    public class Cinema
    {
        public Guid Id { get; set; }
        
        public string Description { get; set; }
        
        public Guid AvatarId { get; set; }
        
        public string Address { get; set; }
        
        public string Name { get; set; }
        
        public Cinema(Guid id,
            string description,
            Guid avatarId,
            string address,
            string name)
        {
            Id = id;
            Description = description;
            AvatarId = avatarId;
            Address = address;
            Name = name;
        }
        
        public ICollection<Session>? Sessions { get; set; }

        public ICollection<MovieCinema>? MovieCinemas { get; set; }
        
        public ICollection<Hall>? Halls { get; set; }
    }
}