using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class MovieFeedback
    {
        public Guid Id { get; set; }
        
        public string Text { get; set; }
        
        public DateTimeOffset Date { get; set; }
        
        public bool Liked { get; set; }
        
        public Guid MovieId { get; set; }
        
        public Guid UserId { get; set; }

        public MovieFeedback(Guid id,
            string text,
            DateTimeOffset date,
            bool liked,
            Guid movieId,
            Guid userId)
        {
            Id = id;
            Text = text;
            Date = date;
            Liked = liked;
            MovieId = movieId;
            UserId = userId;
        }
        
        public Movie? Movie { get; set; }
        
        public User? User { get; set; }
    }
}