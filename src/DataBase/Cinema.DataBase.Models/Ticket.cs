using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class Ticket
    {
        public Guid Id { get; set; }
        
        public int Price { get; set; }
        
        public bool Sold { get; set; }
        
        public Guid? UserId { get; set; }
        
        public Guid SessionId { get; set; }
        
        public Guid SeatId { get; set; }

        public Ticket(Guid id,
            int price,
            bool sold,
            Guid seatId,
            Guid sessionId,
            Guid? userId)
        {
            Id = id;
            Price = price;
            Sold = sold;
            SeatId = seatId;
            SessionId = sessionId;
            UserId = userId;
        }
        
        public User? User { get; set; }
        
        public Session? Session { get; set; }

        public Seat? Seat { get; set; }
    }
}