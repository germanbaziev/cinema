using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class Session
    {
        public Guid Id { get; set; }
        
        public DateTimeOffset StartDate { get; set; }
        
        public Guid MovieId { get; set; }
        
        public Guid HallId { get; set; }
        
        public Guid CinemaId { get; set; }
        
        public Session(Guid id,
            DateTimeOffset startDate,
            Guid hallId,
            Guid movieId,
            Guid cinemaId)
        {
            Id = id;
            StartDate = startDate;
            HallId = hallId;
            MovieId = movieId;
            CinemaId = cinemaId;
        }
        
        public Movie? Movie { get; set; }

        public Hall? Hall { get; set; }
        
        public Cinema? Cinema { get; set; }
        
        public ICollection<Ticket>? Tickets { get; set; }
    }
}