using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class MovieWorkerMovie
    {
        public Guid Id { get; set; }
        
        public Guid MovieWorkerId { get; set; }
        
        public Guid MovieId { get; set; }

        public MovieWorkerMovie(Guid id, Guid movieId, Guid movieWorkerId)
        {
            Id = id;
            MovieId = movieId;
            MovieWorkerId = movieWorkerId;
        }
        
        public MovieWorker? MovieWorker { get; set; }
        
        public Movie? Movie { get; set; }
    }
}