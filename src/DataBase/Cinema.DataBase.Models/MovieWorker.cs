using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class MovieWorker
    {
        public Guid Id { get; set; }
        
        public Guid AvatarId { get; set; }
        
        public string Name { get; set; }
        
        public string Role { get; set; }

        public MovieWorker(Guid id, Guid avatarId, string name, string role)
        {
            Id = id;
            AvatarId = avatarId;
            Name = name;
            Role = role;
        }
        
        public ICollection<MovieWorkerMovie>? MovieWorkerMovies { get; set; }
    }
}