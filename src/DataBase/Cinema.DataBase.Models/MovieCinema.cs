using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class MovieCinema
    { public Guid Id { get; set; }
        
        public Guid CinemaId { get; set; }

        public Guid MovieId { get; set; }
        
        public MovieCinema(Guid id, Guid cinemaId, Guid movieId)
        {
            Id = id;
            CinemaId = cinemaId;
            MovieId = movieId;
        }
        
        public Cinema? Cinemas { get; set; }
        
        public Movie? Movie { get; set; }
    }
}