using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DataBase.Models
{
    public class Hall
    {
        public Guid Id { get; set; }
        
        public int Number { get; set; }
        
        public string Type { get; set; }
        
        public Guid CinemaId { get; set; }

        public Hall(Guid id, int number, string type, Guid cinemaId)
        {
            Id = id;
            Number = number;
            Type = type;
            CinemaId = cinemaId;
        }
        
        public Cinema? Cinema { get; set; }
        
        public ICollection<Seat>? Seats { get; set; }
        
        public ICollection<Session>? Sessions { get; set; }
    }
}