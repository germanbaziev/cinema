namespace Cinema.Core.Repositories;

public interface ICinemaRepository
{
    Task AddCinemaAsync(Guid id,
        string description,
        Guid avatarId,
        string address,
        string name);
    
    Task<List<Models.Cinema>> GetCinemasAsync();

    Task<Models.Cinema?> FindCinemaAsync(Guid id);

    Task DeleteCinemaAsync(Guid id);

    Task<Models.Cinema?> FindCinemaByNameAsync(string cinemaName);

    Task UpdateCinemaAsync(Guid id,
        string description,
        Guid avatarId,
        string address,
        string name);
    
    Task<bool> ExistAsync(Guid id);
}