using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IMovieWorkerRepository
{
    Task AddMovieWorkerAsync(Guid id,
        Guid avatarId,
        string name,
        string role);
    
    Task<List<MovieWorker>> GetMovieWorkersAsync();

    Task<MovieWorker?> FindMovieWorkerAsync(Guid id);

    Task DeleteMovieWorkerAsync(Guid id);

    Task UpdateMovieWorkerAsync(Guid id,
        Guid avatarId,
        string name,
        string role);

    Task<bool> ExistAsync(Guid id);
}