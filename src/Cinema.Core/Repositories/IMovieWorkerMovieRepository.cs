using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IMovieWorkerMovieRepository
{
    Task AddMovieWorkerMovieAsync(Guid id,
        Guid movieWorkerId,
        Guid movieId);

    Task UpdateMovieWorkerMovieAsync(Guid id, Guid movieWorkerId, Guid movieId);
}