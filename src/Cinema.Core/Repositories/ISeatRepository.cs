using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface ISeatRepository
{
    Task AddSeatAsync(Guid id,
        string type,
        int row,
        int number,
        Guid hallId);

    Task<List<Seat>> GetSeatsAsync();

    Task<Seat?> FindSeatAsync(Guid id);

    Task DeleteSeatAsync(Guid id);

    Task UpdateSeatAsync(Guid id,
        string type,
        int row,
        int number,
        Guid hallId);
    
    Task<bool> ExistAsync(Guid id);
}