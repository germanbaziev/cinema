using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IAuthRepository
{
    Task<User?> GetUserAsync(string login, string password);

    Task<bool> ExistUserAsync(string login, string password);
}