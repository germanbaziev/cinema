using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IMovieRepository
{
    Task AddMovieRepositories(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director);

    Task<List<Movie>> GetMoviesAsync();

    Task<Movie?> FindMovieAsync(Guid id);

    Task DeleteMovieAsync(Guid id);

    Task<Movie?> FindMovieByName(string movieName);

    Task UpdateMovieAsync(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director);
    
    Task<bool> ExistAsync(Guid id);
}