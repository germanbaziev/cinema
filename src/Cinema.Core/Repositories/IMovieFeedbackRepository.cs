using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IMovieFeedbackRepository
{
    Task AddMovieFeedBackAsync(Guid id,
        string text,
        DateTimeOffset date,
        bool liked,
        Guid movieId,
        Guid userId);

    Task UpdateMovieFeedbackAsync(Guid id,
        string text,
        DateTimeOffset date,
        bool liked,
        Guid movieId,
        Guid userId);

    Task<List<MovieFeedback>> GetMovieFeedbacksAsync();

    Task<MovieFeedback?> GetMovieFeedbackAsync(Guid id);

    Task<bool> ExistsAsync(Guid id);

    Task RemoveMovieFeedback(Guid id);
}