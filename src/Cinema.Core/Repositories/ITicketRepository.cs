using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface ITicketRepository
{
    Task AddTicketRepositories(Guid id,
        int price,
        bool sold,
        Guid sessionId,
        Guid seatId,
        Guid? userId = null);

    Task<List<Ticket>> GetTicketsAsync();

    Task<Ticket?> FindTicketAsync(Guid id);

    Task<Ticket?> FindTicketBySeatAndSessionIdAsync(Guid seatId, Guid sessionId);

    Task DeleteTicketAsync(Guid id);

    Task UpdateTicketAsync(Guid id,
        int price,
        bool sold,
        Guid sessionId,
        Guid seatId,
        Guid? userId = null);
    
    Task<bool> ExistAsync(Guid id);
}