using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IUserRepository
{
    Task AddUserAsync(Guid id,
        string password,
        string login,
        string permissions,
        string? telegram = null,
        string? phone = null,
        string? email = null,
        Guid? avatarId = null);

    Task<bool> AuthUser(string name, string password);

    Task<User?> GetUserData(string name, string password);

    Task<List<User>> GetUsersAsync();

    Task<User?> FindUserAsync(Guid id);

    Task DeleteUserAsync(Guid id);

    Task UpdateUserAsync(Guid id,
        string password,
        string login,
        string permissions,
        string? telegram = null,
        string? phone = null,
        string? email = null,
        Guid? avatarId = null);
    
    public Task<bool> ExistAsync(Guid id);
}