using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface IHallRepository
{
    Task AddHallAsync(Guid id,
        int number,
        string type,
        Guid cinemaId);

    Task<List<Hall>> GetHallsAsync();

    Task<Hall?> FindHallAsync(Guid id);

    Task DeleteHallAsync(Guid id);

    Task UpdateHallAsync(Guid id,
        int number,
        string type,
        Guid cinemaId);
    
    Task<bool> ExistAsync(Guid id);
}