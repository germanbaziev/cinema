using File = Cinema.Core.Models.File;

namespace Cinema.Core.Repositories;

public interface IFileRepository
{
    Task AddFileAsync(Guid id, string name, byte[]? data = null);

    Task<File?> FindFileAsync(Guid id);
}