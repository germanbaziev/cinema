namespace Cinema.Core.Repositories;

public interface IMovieCinemaRepository
{
    Task AddMovieCinemaAsync(Guid id,
        Guid cinemaId,
        Guid movieId);
}