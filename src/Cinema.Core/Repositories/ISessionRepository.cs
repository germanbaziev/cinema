using Cinema.Core.Models;

namespace Cinema.Core.Repositories;

public interface ISessionRepository
{
    Task AddSessionAsync(Guid id,
        DateTimeOffset startDate,
        Guid movieId,
        Guid hallId,
        Guid cinemaId);

    Task<List<Session>> GetSessionsByCinemaAndMovieIdAsync(Guid cinemaId, Guid movieId);
    
    Task<List<Session>> GetSessionsAsync();

    Task<Session?> FindSessionAsync(Guid id);

    Task DeleteSessionAsync(Guid id);

    Task UpdateSessionAsync(Guid id,
        DateTimeOffset startDate,
        Guid movieId,
        Guid hallId,
        Guid cinemaId);
    
    Task<bool> ExistAsync(Guid id);
}