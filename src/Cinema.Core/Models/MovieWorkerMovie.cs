namespace Cinema.Core.Models;

public class MovieWorkerMovie
{
    public Guid Id { get; set; }
    
    public Guid MovieWorkerId { get; set; }

    public Guid MovieId { get; set; }
    
    public MovieWorkerMovie(Guid id, Guid movieWorkerId, Guid movieId)
    {
        Id = id;
        MovieWorkerId = movieWorkerId;
        MovieId = movieId;
    }

}