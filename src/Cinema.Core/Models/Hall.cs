namespace Cinema.Core.Models;

public class Hall
{
    public Guid Id { get; set; }
    
    public int Number { get; set; }
    
    public string Type { get; set; }
    
    public Guid CinemaId { get; set; }

    public List<Seat> Seats { get; set; }
    
    public Hall(Guid id,
        int number,
        string type,
        Guid cinemaId,
        List<Seat> seats)
    {
        Id = id;
        Number = number;
        Type = type;
        CinemaId = cinemaId;
        Seats = seats;
    }
}