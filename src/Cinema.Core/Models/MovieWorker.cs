using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Cinema.Core.Models;

public class MovieWorker
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid AvatarId { get; set; }
    
    [Required]
    [DataMember(Name = "name")]
    public string Name { get; set; }
    
    [Required]
    [DataMember(Name = "role")]
    public string Role { get; set; }
    
    [Required]
    [DataMember(Name = "movies")]
    public List<MovieWorkerMovie> MovieWorkerMovies { get; set; }
    
    public MovieWorker(Guid id,
        Guid avatarId,
        string name,
        string role,
        List<MovieWorkerMovie> movieWorkerMovies)
    {
        Id = id;
        AvatarId = avatarId;
        Name = name;
        Role = role;
        MovieWorkerMovies = movieWorkerMovies;
    }
}