namespace Cinema.Core.Models;

public class User
{
    public Guid Id { get; set; }
    
    public Guid? AvatarId { get; set; }
    
    public string Password { get; set; }
    
    public string Login { get; set; }
    
    public string? Telegram { get; set; }
    
    public string? Phone { get; set; }
    
    public string? Email { get; set; }
    
    public string Permissions { get; set; }
    
    public List<Ticket> Tickets { get; set; }
    
    public User(Guid id,
        Guid? avatarId,
        string password,
        string login,
        string? telegram,
        string? phone,
        string? email,
        string permissions,
        List<Ticket> tickets)
    {
        Id = id;
        AvatarId = avatarId;
        Password = password;
        Login = login;
        Telegram = telegram;
        Phone = phone;
        Email = email;
        Permissions = permissions;
        Tickets = tickets;
    }
}