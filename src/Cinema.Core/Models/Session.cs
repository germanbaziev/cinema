namespace Cinema.Core.Models;

public class Session
{
    public Guid Id { get; set; }
    
    public DateTimeOffset StartDate { get; set; }
    
    public Guid MovieId { get; set; }
    
    public Guid HallId { get; set; }
    
    public Guid CinemaId { get; set; }
    
    public List<Ticket> Tickets { get; set; }

    public Session(Guid id,
        DateTimeOffset startDate,
        Guid movieId,
        Guid hallId,
        List<Ticket> tickets,
        Guid cinemaId)
    {
        Id = id;
        StartDate = startDate;
        MovieId = movieId;
        HallId = hallId;
        Tickets = tickets;
        CinemaId = cinemaId;
    }
}