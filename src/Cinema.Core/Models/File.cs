namespace Cinema.Core.Models;

public class File
{
    public Guid Id { get; set; }
    
    public byte[]? Data { get; set; }

    public string Name { get; set; }
    
    public File(Guid id, byte[]? data, string name)
    {
        Id = id;
        Data = data;
        Name = name;
    }
}