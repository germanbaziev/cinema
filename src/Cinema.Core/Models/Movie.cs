namespace Cinema.Core.Models;

public class Movie
{
    public Guid Id { get; set; }
    
    public Guid TrailerId { get; set; }
    
    public Guid AvatarId { get; set; }
    
    public string Name { get; set; }
    
    public string Description { get; set; }
    
    public string Country { get; set; }
    
    public int AgeLimit { get; set; }
    
    public string Director { get; set; }
    
    public List<Session> Sessions { get; set; }
    
    public List<MovieWorkerMovie> MovieWorkerMovies { get; set; }
    
    public List<MovieFeedback> MovieFeedbacks { get; set; }
    
    public List<MovieCinema> MovieCinemas { get; set; }

    public Movie(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director,
        List<Session> sessions,
        List<MovieWorkerMovie> movieWorkerMovies,
        List<MovieFeedback> movieFeedbacks,
        List<MovieCinema> movieCinemas)
    {
        Id = id;
        TrailerId = trailerId;
        AvatarId = avatarId;
        Name = name;
        Description = description;
        Country = country;
        AgeLimit = ageLimit;
        Director = director;
        Sessions = sessions;
        MovieWorkerMovies = movieWorkerMovies;
        MovieFeedbacks = movieFeedbacks;
        MovieCinemas = movieCinemas;
    }
}