namespace Cinema.Core.Models;

public class MovieCinema
{
    public Guid Id { get; set; }
    
    public Guid CinemaId { get; set; }
    
    public Guid MovieId { get; set; }

    public MovieCinema(Guid id, Guid cinemaId, Guid movieId)
    {
        Id = id;
        CinemaId = cinemaId;
        MovieId = movieId;
    }
}