namespace Cinema.Core.Models;

public class Cinema
{
    public Guid Id { get; set; }
    
    public string Description { get; set; }
    
    public Guid AvatarId { get; set; }
    
    public string Address { get; set; }
    
    public string Name { get; set; }
    
    public List<MovieCinema> MovieCinemas { get; set; }
    
    public List<Hall> Halls { get; set; }

    public Cinema(Guid id,
        string description,
        Guid avatarId,
        string address,
        string name,
        List<MovieCinema> movieCinemas,
        List<Hall> halls)
    {
        Id = id;
        Description = description;
        AvatarId = avatarId;
        Address = address;
        Name = name;
        MovieCinemas = movieCinemas;
        Halls = halls;
    }
}