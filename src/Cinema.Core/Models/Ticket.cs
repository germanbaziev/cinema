namespace Cinema.Core.Models;

public class Ticket
{
    public Guid Id { get; set; }
    
    public int Price { get; set; }
    
    public bool Sold { get; set; }
    
    public Guid? UserId { get; set; }
    
    public Guid SessionId { get; set; }
    
    public Guid SeatId { get; set; }

    public Ticket(Guid id,
        int price,
        bool sold,
        Guid? userId,
        Guid sessionId,
        Guid seatId)
    {
        Id = id;
        Price = price;
        Sold = sold;
        UserId = userId;
        SessionId = sessionId;
        SeatId = seatId;
    }
}