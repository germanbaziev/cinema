namespace Cinema.Core.Models;

public class MovieFeedback
{
    public Guid Id { get; set; }
    
    public string Text { get; set; }
    
    public DateTimeOffset Date { get; set; }
    
    public bool Liked { get; set; }

    public Guid MovieId { get; set; }
    
    public Guid UserId { get; set; }

    public MovieFeedback(Guid id,
        string text,
        DateTimeOffset date,
        bool liked,
        Guid movieId,
        Guid userId)
    {
        Id = id;
        Text = text;
        Date = date;
        Liked = liked;
        MovieId = movieId;
        UserId = userId;
    }
}