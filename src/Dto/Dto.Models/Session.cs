using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class Session
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "startDate")]
    public DateTimeOffset StartDate { get; set; }
    
    [Required]
    [DataMember(Name = "movieId")]
    public Guid MovieId { get; set; }
    
    [Required]
    [DataMember(Name = "hallId")]
    public Guid HallId { get; set; }
    
    [Required]
    [DataMember(Name = "cinemaId")]
    public Guid CinemaId { get; set; }
    
    [Required]
    [DataMember(Name = "tickets")]
    public List<Ticket> Tickets { get; set; }

    public Session(Guid id,
        DateTimeOffset startDate,
        Guid movieId,
        Guid hallId,
        Guid cinemaId,
        List<Ticket> tickets)
    {
        Id = id;
        StartDate = startDate;
        MovieId = movieId;
        HallId = hallId;
        Tickets = tickets;
        CinemaId = cinemaId;
    }
}