using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class MovieGet
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }

    [Required]
    [DataMember(Name = "TrailerId")]
    public Guid TrailerId { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid AvatarId { get; set; }
    
    [Required]
    [DataMember(Name = "name")]
    public string Name { get; set; }
    
    [Required]
    [DataMember(Name = "description")]
    public string Description { get; set; }
    
    [Required]
    [DataMember(Name = "country")]
    public string Country { get; set; }
    
    [Required]
    [DataMember(Name = "ageLimit")]
    public int AgeLimit { get; set; }
    
    [Required]
    [DataMember(Name = "director")]
    public string Director { get; set; }
    
    [Required]
    [DataMember(Name = "movieWorkers")]
    public List<MovieWorkerMovie> MovieWorkerMovie { get; set; }
    
    [Required]
    [DataMember(Name = "cinemas")]
    public List<MovieCinema> MovieCinemas { get; set; }
    
    [Required]
    [DataMember(Name = "sessions")]
    public List<Session> Sessions { get; set; }

    public MovieGet(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director,
        List<MovieWorkerMovie> movieWorkerMovies,
        List<MovieCinema> movieCinemas,
        List<Session> sessions)
    {
        Id = id;
        TrailerId = trailerId;
        AvatarId = avatarId;
        Name = name;
        Description = description;
        Country = country;
        AgeLimit = ageLimit;
        Director = director;
        MovieWorkerMovie = movieWorkerMovies;
        MovieCinemas = movieCinemas;
        Sessions = sessions;
    }
}

public class MoviePut
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }

    [Required]
    [DataMember(Name = "TrailerId")]
    public Guid TrailerId { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid AvatarId { get; set; }
    
    [Required]
    [DataMember(Name = "name")]
    public string Name { get; set; }
    
    [Required]
    [DataMember(Name = "description")]
    public string Description { get; set; }
    
    [Required]
    [DataMember(Name = "country")]
    public string Country { get; set; }
    
    [Required]
    [DataMember(Name = "ageLimit")]
    public int AgeLimit { get; set; }
    
    [Required]
    [DataMember(Name = "director")]
    public string Director { get; set; }

    public MoviePut(Guid id,
        Guid trailerId,
        Guid avatarId,
        string name,
        string description,
        string country,
        int ageLimit,
        string director)
    {
        Id = id;
        TrailerId = trailerId;
        AvatarId = avatarId;
        Name = name;
        Description = description;
        Country = country;
        AgeLimit = ageLimit;
        Director = director;
    }
}