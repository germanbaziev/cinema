using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class MovieWorkerMovie
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "movieWorkerId")]
    public Guid MovieWorkerId { get; set; }
    
    [Required]
    [DataMember(Name = "movieId")]
    public Guid MovieId { get; set; }

    public MovieWorkerMovie(Guid id, Guid movieWorkerId, Guid movieId)
    {
        Id = id;
        MovieWorkerId = movieWorkerId;
        MovieId = movieId;
    }
}