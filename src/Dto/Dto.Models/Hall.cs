using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class Hall
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "number")]
    public int Number { get; set; }
    
    [Required]  
    [DataMember(Name = "type")]
    public string Type { get; set; }
    
    [Required]
    [DataMember(Name = "cinemaId")]
    public Guid CinemaId { get; set; }
    
    [Required]
    [DataMember(Name = "seats")]
    public List<Seat> Seats { get; set; }

    public Hall(Guid id, int number, string type, Guid cinemaId, List<Seat> seats)
    {
        Id = id;
        Number = number;
        Type = type;
        CinemaId = cinemaId;
        Seats = seats;
    }
}