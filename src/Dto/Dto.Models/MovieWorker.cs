using System;
using System.Collections.Generic;

namespace Dto.Models;

public class MovieWorker
{
    public Guid Id { get; set; }
    
    public Guid AvatarId { get; set; }
    
    public string Name { get; set; }
    
    public string Role { get; set; }
    
    public List<MovieWorkerMovie> MovieWorkerMovies { get; set; }

    public MovieWorker(Guid id,
        Guid avatarId,
        string name,
        string role,
        List<MovieWorkerMovie> movieWorkerMovies)
    {
        Id = id;
        AvatarId = avatarId;
        Name = name;
        Role = role;
        MovieWorkerMovies = movieWorkerMovies;
    }
}