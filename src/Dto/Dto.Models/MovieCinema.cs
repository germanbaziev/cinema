using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class MovieCinema
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "cinemaId")]
    public Guid CinemaId { get; set; }
    
    [Required]
    [DataMember(Name = "movieId")]
    public Guid MovieId { get; set; }

    public MovieCinema(Guid id, Guid cinemaId, Guid movieId)
    {
        Id = id;
        CinemaId = cinemaId;
        MovieId = movieId;
    }
}