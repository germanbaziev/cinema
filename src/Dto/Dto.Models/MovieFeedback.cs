using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class MovieFeedback
{
    [Required]
    [DataMember(Name = "id")]
    public Guid id { get; set; }
    
    [Required]
    [DataMember(Name = "text")]
    public string Text { get; set; }
    
    [Required]
    [DataMember(Name = "date")]
    public DateTimeOffset Date { get; set; }

    [Required]
    [DataMember(Name = "liked")]
    public bool Liked { get; set; }
    
    [Required]
    [DataMember(Name = "movieId")]
    public Guid MovieId { get; set; }
    
    [Required]
    [DataMember(Name = "UserId")]
    public Guid UserId { get; set; }

    public MovieFeedback(Guid id,
        string text,
        DateTimeOffset date,
        bool liked,
        Guid movieId,
        Guid userId)
    {
        this.id = id;
        Text = text;
        Date = date;
        Liked = liked;
        MovieId = movieId;
        UserId = userId;
    }
}