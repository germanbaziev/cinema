using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class Ticket
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "price")]
    public int Price { get; set; }
    
    [Required]
    [DataMember(Name = "sold")]
    public bool Sold { get; set; }
    
    [Required]
    [DataMember(Name = "userId")]
    public Guid? UserId { get; set; }
    
    [Required]
    [DataMember(Name = "sessionId")]
    public Guid SessionId { get; set; }
    
    [Required]
    [DataMember(Name = "seatId")]
    public Guid SeatId { get; set; }

    public Ticket(Guid id,
        int price,
        bool sold,
        Guid? userId,
        Guid sessionId,
        Guid seatId)
    {
        Id = id;
        Price = price;
        Sold = sold;
        UserId = userId;
        SessionId = sessionId;
        SeatId = seatId;
    }
}