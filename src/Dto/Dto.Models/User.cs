using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class UserGet
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid? AvatarId { get; set; }
    
    [Required]
    [DataMember(Name = "password")]
    public string Password { get; set; }
    
    [Required]
    [DataMember(Name = "login")]
    public string Login { get; set; }
    
    [Required]
    [DataMember(Name = "telegram")]
    public string Telegram { get; set; }
    
    [Required]
    [DataMember(Name = "phone")]
    public string Phone { get; set; }
    
    [Required]
    [DataMember(Name = "email")]
    public string Email { get; set; }
    
    [Required]
    [DataMember(Name = "permissions")]
    public string Permissions { get; set; }
    
    [Required]
    [DataMember(Name = "tickets")]
    public List<Ticket> Tickets { get; set; }

    public UserGet(Guid id, Guid? avatarId,
        string password,
        string login,
        string telegram,
        string phone,
        string email,
        string permissions,
        List<Ticket> tickets)
    {
        Id = id;
        AvatarId = avatarId;
        Password = password;
        Login = login;
        Telegram = telegram;
        Phone = phone;
        Email = email;
        Permissions = permissions;
        Tickets = tickets;
    }
}

public class UserPut
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid? AvatarId { get; set; }
    
    [Required]
    [DataMember(Name = "password")]
    public string Password { get; set; }
    
    [Required]
    [DataMember(Name = "login")]
    public string Login { get; set; }
    
    [Required]
    [DataMember(Name = "telegram")]
    public string Telegram { get; set; }
    
    [Required]
    [DataMember(Name = "phone")]
    public string Phone { get; set; }
    
    [Required]
    [DataMember(Name = "email")]
    public string Email { get; set; }
    
    [Required]
    [DataMember(Name = "permissions")]
    public string Permissions { get; set; }

    public UserPut(Guid id, Guid? avatarId,
        string password,
        string login,
        string telegram,
        string phone,
        string email,
        string permissions)
    {
        Id = id;
        AvatarId = avatarId;
        Password = password;
        Login = login;
        Telegram = telegram;
        Phone = phone;
        Email = email;
        Permissions = permissions;
    }
}