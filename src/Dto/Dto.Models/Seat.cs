using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class Seat
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "type")]
    public string Type { get; set; }
    
    [Required]
    [DataMember(Name = "row")]
    public int Row { get; set; }
    
    [Required]
    [DataMember(Name = "Number")]
    public int Number { get; set; }
    
    [Required]
    [DataMember(Name = "hallId")]
    public Guid HallId { get; set; }

    public Seat(Guid id, string type, int row, int number, Guid hallId)
    {
        Id = id;
        Type = type;
        Row = row;
        Number = number;
        HallId = hallId;
    }
}