using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class CinemaGet
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "description")]
    public string Description { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid AvatarId { get; set; }

    [Required]
    [DataMember(Name = "address")]
    public string Address { get; set; }

    [Required]
    [DataMember(Name = "name")]
    public string Name { get; set; }
    
    [Required]
    [DataMember(Name = "movies")]
    public List<MovieCinema> MovieCinemas { get; set; }
    
    [Required]
    [DataMember(Name = "hall")]
    public List<Hall> Halls { get; set; }

    public CinemaGet(Guid id,
        string description,
        Guid avatarId, string address,
        string name,
        List<MovieCinema> movieCinemas,
        List<Hall> halls)
    {
        Id = id;
        Description = description;
        AvatarId = avatarId;
        Address = address;
        Name = name;
        MovieCinemas = movieCinemas;
        Halls = halls;
    }
}

public class CinemaPut
{
    [Required]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [Required]
    [DataMember(Name = "description")]
    public string Description { get; set; }
    
    [Required]
    [DataMember(Name = "avatarId")]
    public Guid AvatarId { get; set; }

    [Required]
    [DataMember(Name = "address")]
    public string Address { get; set; }

    [Required]
    [DataMember(Name = "name")]
    public string Name { get; set; }
    
    [Required]
    [DataMember(Name = "movies")]
    public List<MovieCinema> MovieCinemas { get; set; }

    public CinemaPut(Guid id,
        string description,
        Guid avatarId, string address,
        string name,
        List<MovieCinema> movieCinemas)
    {
        Id = id;
        Description = description;
        AvatarId = avatarId;
        Address = address;
        Name = name;
        MovieCinemas = movieCinemas;
    }
}