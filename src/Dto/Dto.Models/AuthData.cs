using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dto.Models;

public class AuthData
{
    [Required]
    [DataMember(Name = "login")]
    public string Login { get; set; }
    
    [Required]
    [DataMember(Name = "password")]
    public string Password { get; set; }
}