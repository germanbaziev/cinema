using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Cinema.Server.JwtAuth;

public static class AuthOptions
{
    public const string ISSUER = "IntertextServer";
    public const string AUDIENCE = "IntertextClient";
    private const string KEY = "c84f18a2-c6c7-4850-be15-93f9cbaef3b3";
    public const int LIFETIME = 0;
    public static SymmetricSecurityKey GetSymmetricSecurityKey()
    {
        return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
    }
}