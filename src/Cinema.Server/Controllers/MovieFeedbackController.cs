using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/movieFeedback")]
public class MovieFeedbackController : ControllerBase
{
    private readonly IMovieFeedbackRepository _movieFeedbackRepository;
    private readonly ILogger<MovieFeedbackController> _logger;

    public MovieFeedbackController(IMovieFeedbackRepository movieFeedbackRepository, ILogger<MovieFeedbackController> logger)
    {
        _movieFeedbackRepository = movieFeedbackRepository;
        _logger = logger;
    }

    [Authorize(Roles = "User")]
    [HttpPut]
    public async Task<IActionResult> PutMovieFeedbackAsync([FromBody] MovieFeedback movieFeedback)
    {
        try
        {
            if(!await _movieFeedbackRepository.ExistsAsync(movieFeedback.id))
            {
                await _movieFeedbackRepository.AddMovieFeedBackAsync(movieFeedback.id,
                    movieFeedback.Text,
                    movieFeedback.Date,
                    movieFeedback.Liked,
                    movieFeedback.MovieId,
                    movieFeedback.UserId);
            }
            else
            {
                await _movieFeedbackRepository.UpdateMovieFeedbackAsync(movieFeedback.id,
                    movieFeedback.Text,
                    movieFeedback.Date,
                    movieFeedback.Liked,
                    movieFeedback.MovieId,
                    movieFeedback.UserId);
            }

            return Ok();
        }
        catch(Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieFeedbackAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet]
    public async Task<IActionResult> GetMovieFeedbacksAsync()
    {
        try
        {
            var movieFeedbacks = await _movieFeedbackRepository.GetMovieFeedbacksAsync();

            return Ok(movieFeedbacks.Select(MovieFeedbackConverter.ConvertMovieFeedbackToDto));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieFeedbackAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("{movieFeedbackId:guid}")]
    public async Task<IActionResult> GetMovieFeedbackAsync([FromRoute] Guid movieFeedbackId)
    {
        try
        {
            var movieFeedback = await _movieFeedbackRepository.GetMovieFeedbackAsync(movieFeedbackId);

            if (movieFeedback is null)
                return NotFound();

            return Ok(MovieFeedbackConverter.ConvertMovieFeedbackToDto(movieFeedback));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieFeedbackAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin, User")]
    [HttpDelete("{movieFeedbackId:guid}")]
    public async Task<IActionResult> DeleteMovieFeedbackAsync(Guid movieFeedbackId)
    {
        try
        {
            await _movieFeedbackRepository.RemoveMovieFeedback(movieFeedbackId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieFeedbackAsync));
            
            return StatusCode(500);
        }
    }
}