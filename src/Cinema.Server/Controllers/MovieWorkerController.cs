using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[ApiController]
[Route("/api/movieWorker")]
public class MovieWorkerController : ControllerBase
{
    private readonly IMovieWorkerRepository _movieWorkerRepositories;
    private readonly IMovieWorkerMovieRepository _movieWorkerMovieRepositories;
    private readonly ILogger<MovieWorkerController> _logger;

    public MovieWorkerController(IMovieWorkerRepository movieWorkerRepositories,
        IMovieWorkerMovieRepository movieWorkerMovieRepositories,
        ILogger<MovieWorkerController> logger)
    {
        _movieWorkerRepositories = movieWorkerRepositories;
        _movieWorkerMovieRepositories = movieWorkerMovieRepositories;
        _logger = logger;
    }

    [HttpPut]
    public async Task<IActionResult> PutMovieWorkerAsync([FromBody] MovieWorker movieWorker)
    {
        try
        {
            if(!await _movieWorkerRepositories.ExistAsync(movieWorker.Id))
            {
                await _movieWorkerRepositories.AddMovieWorkerAsync(movieWorker.Id,
                    movieWorker.AvatarId,
                    movieWorker.Name,
                    movieWorker.Role);

                foreach (var movieWorkerMovie in movieWorker.MovieWorkerMovies)
                {
                    await _movieWorkerMovieRepositories.AddMovieWorkerMovieAsync(Guid.NewGuid(),
                        movieWorker.Id,
                        movieWorkerMovie.MovieId);
                }               
            }
            else
            {
                await _movieWorkerRepositories.UpdateMovieWorkerAsync(movieWorker.Id,
                    movieWorker.AvatarId,
                    movieWorker.Name,
                    movieWorker.Role);
                
                foreach (var movieWorkerMovie in movieWorker.MovieWorkerMovies)
                {
                    await _movieWorkerMovieRepositories.UpdateMovieWorkerMovieAsync(movieWorkerMovie.Id,
                        movieWorker.Id,
                        movieWorkerMovie.MovieId);
                }     
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieWorkerAsync));
            
            return StatusCode(500);
        }
    }
    
    [HttpGet]
    public async Task<IActionResult> GetMovieWorkersAsync()
    {
        try
        {
            var movieWorkers =  await _movieWorkerRepositories.GetMovieWorkersAsync();
    
            return Ok(movieWorkers.Select(MovieWorkerConverter.ConvertMovieWorkerToDto).ToList());
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetMovieWorkersAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("{movieWorkerId:guid}")]
    public async Task<IActionResult> GetMovieWorkerAsync([FromRoute] Guid movieWorkerId)
    {
        try
        {
            var movieWorker = await _movieWorkerRepositories.FindMovieWorkerAsync(movieWorkerId);

            if (movieWorker != null) return Ok(MovieWorkerConverter.ConvertMovieWorkerToDto(movieWorker));
            else
            {
                return StatusCode(404);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetMovieWorkersAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete("{movieWorkerId:guid}")]
    public async Task<IActionResult> DeleteMovieWorkerAsync([FromRoute] Guid movieWorkerId)
    {
        try
        {
            await _movieWorkerRepositories.DeleteMovieWorkerAsync(movieWorkerId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(DeleteMovieWorkerAsync));
            
            return StatusCode(500);
        }
    }
}

