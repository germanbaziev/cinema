using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/movie")]
public class MovieController : ControllerBase
{
    private readonly IMovieRepository _movieRepositories;
    private readonly IMovieCinemaRepository _movieCinemaRepository;
    private readonly ILogger<MovieController> _logger;

    public MovieController(IMovieRepository movieRepositories, ILogger<MovieController> logger, IMovieCinemaRepository movieCinemaRepository)
    {
        _movieRepositories = movieRepositories;
        _logger = logger;
        _movieCinemaRepository = movieCinemaRepository;
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutMovieAsync([FromBody] MoviePut movie)
    {
        try
        {
            if(!await _movieRepositories.ExistAsync(movie.Id))
            {
                await _movieRepositories.AddMovieRepositories(movie.Id,
                    movie.TrailerId,
                    movie.AvatarId,
                    movie.Name,
                    movie.Description,
                    movie.Country,
                    movie.AgeLimit,
                    movie.Director);
            }
            else
            {
                await _movieRepositories.UpdateMovieAsync(movie.Id,
                    movie.TrailerId,
                    movie.AvatarId,
                    movie.Name,
                    movie.Description,
                    movie.Country,
                    movie.AgeLimit,
                    movie.Director);
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutMovieAsync));
            
            return StatusCode(500);
        }
    }

    
    [HttpGet]
    public async Task<IActionResult> GetMoviesAsync()
    {
        try
        {
            var coreMovies = await _movieRepositories.GetMoviesAsync();

            return Ok(coreMovies.Select(MovieGetConverter.ConvertMovieGetToDto).ToList());
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetMoviesAsync));
            
            return StatusCode(500);
        }
    }
    
    [HttpGet("{movieId:guid}")]
    public async Task<IActionResult> GetMovieAsync([FromRoute] Guid movieId)
    {
        try
        {
            var coreMovie = await _movieRepositories.FindMovieAsync(movieId);

            if (coreMovie is null)
                return NotFound();

            return Ok(MovieGetConverter.ConvertMovieGetToDto(coreMovie));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetMovieAsync));
            
            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete("{movieId:guid}")]
    public async Task<IActionResult> DeleteMovieAsync([FromRoute] Guid movieId)
    {
        try
        {
            await _movieRepositories.DeleteMovieAsync(movieId);
            
            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(DeleteMovieAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("{movieName}")]
    public async Task<IActionResult> GetMovieByName([FromRoute] string movieName)
    {
        try
        {
            var coreMovie = await _movieRepositories.FindMovieByName(movieName);

            if (coreMovie is null)
                return NotFound();

            return Ok(MovieGetConverter.ConvertMovieGetToDto(coreMovie));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetMovieByName));
            
            return StatusCode(500);
        }
    }
    
}