using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[ApiController]
[Route("/api/file")]
public class FileController : ControllerBase
{   
    private readonly IFileRepository _fileRepository;

    private readonly ILogger<FileController> _logger;

    public FileController(IFileRepository fileRepository, ILogger<FileController> logger)
    {
        _fileRepository = fileRepository;
        _logger = logger;
    }

    [Authorize(Roles = "User")]
    [HttpGet]
    [Route("{fileId:guid}")]
    public async Task<IActionResult> GetFileAsync([FromRoute] Guid fileId)
    {
        try
        {
            var file = await _fileRepository.FindFileAsync(fileId);
            
            if(file == null)
                return NotFound();

            return Ok(FileConverter.ConvertFileToDto(file));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}.", nameof(GetFileAsync));

            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "User")]
    [HttpPost]
    public async Task<IActionResult> PostFileAsync(IFormFile inputFile, string name)
    {
        try
        {
            var fileId = Guid.NewGuid();
            var fileData = await GetContentFromFileAsync(inputFile);

            await _fileRepository.AddFileAsync(fileId, name, fileData);
            
            return Ok(fileId);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}.", nameof(PostFileAsync));

            return StatusCode(500);
        }
    }

    private static async Task<byte[]> GetContentFromFileAsync(IFormFile inputFile)
    {
        await using var ms = new MemoryStream();
        await inputFile.CopyToAsync(ms);
        
        return ms.ToArray();
    }
}