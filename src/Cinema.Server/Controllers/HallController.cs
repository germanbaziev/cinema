using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/hall")]
public class HallController : ControllerBase
{
    private readonly IHallRepository _hallRepository;
    private readonly ILogger<HallController> _logger;
    private readonly ISeatRepository _seatRepository;

    public HallController(IHallRepository hallRepository, ILogger<HallController> logger, ISeatRepository seatRepository)
    {
        _hallRepository = hallRepository;
        _logger = logger;
        _seatRepository = seatRepository;
    }
    
    [Authorize(Roles = "Admin")]
    [HttpGet]
    public async Task<IActionResult> GetHallsAsync()
    {
        try
        {
            var coreHalls = await _hallRepository.GetHallsAsync();

            return Ok(coreHalls.Select(HallConverter.ConvertHallToDto).ToList());
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetHallsAsync));

            return StatusCode(500);
        }
    }
    
    [HttpGet("{hallId:guid}")]
    public async Task<IActionResult> GetHallAsync([FromRoute] Guid hallId)
    {
        try
        {
            var coreHalls = await _hallRepository.FindHallAsync(hallId);

            if (coreHalls is null)
                return NotFound();
            
            return Ok(HallConverter.ConvertHallToDto(coreHalls));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetHallAsync));

            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete("{hallId:guid}")]
    public async Task<IActionResult> DeleteHallsAsync([FromRoute] Guid hallId)
    {
        try
        {
            await _hallRepository.DeleteHallAsync(hallId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(DeleteHallsAsync));

            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutHallAsync([FromBody] Hall hall)
    {
        try
        {
            if (!await _hallRepository.ExistAsync(hall.Id))
            {
                await _hallRepository.AddHallAsync(hall.Id,
                    hall.Number,
                    hall.Type,
                    hall.CinemaId);

                foreach (var seat in hall.Seats)
                {
                    await _seatRepository.AddSeatAsync(seat.Id,
                        seat.Type,
                        seat.Row,
                        seat.Number,
                        hall.Id);
                }
            }
            else
            {
                await _hallRepository.UpdateHallAsync(hall.Id,
                    hall.Number,
                    hall.Type,
                    hall.CinemaId);
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutHallAsync));

            return StatusCode(500);
        }
    }
}