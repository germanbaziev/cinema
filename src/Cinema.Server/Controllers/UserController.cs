using System.Security.Cryptography.X509Certificates;
using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[ApiController]
[Route("/api/user")]
public class UserController : ControllerBase
{
    private readonly IUserRepository _userRepository;
    private readonly ILogger<UserController> _logger;

    public UserController(IUserRepository userRepository, ILogger<UserController> logger)
    {
        _userRepository = userRepository;
        _logger = logger;
    }

    [Authorize(Roles = "Admin")]
    [HttpGet]
    public async Task<IActionResult> GetUsersAsync()
    {
        try
        {
            var coreUser =  await _userRepository.GetUsersAsync();

            return Ok(coreUser.Select(UserGetConverter.ConvertUserGetToDto).ToList());
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetUsersAsync));
            
            return StatusCode(500);
        }
    }

    [HttpPut]
    public async Task<IActionResult> PutUserAsync([FromBody] UserPut user)
    {
        try
        {
            if (!await _userRepository.ExistAsync(user.Id))
            {
                await _userRepository.AddUserAsync(user.Id,
                    user.Password,
                    user.Login,
                    user.Permissions,
                    user.Telegram,
                    user.Phone,
                    user.Email,
                    user.AvatarId);
            }
            else
            {
                await _userRepository.UpdateUserAsync(user.Id,
                    user.Password,
                    user.Login,
                    user.Permissions,
                    user.Telegram,
                    user.Phone,
                    user.Email,
                    user.AvatarId);
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetUsersAsync));
            
            return StatusCode(500);
        }

    }
    
    [HttpGet("{userId:guid}")]
    public async Task<IActionResult> GetUserAsync([FromRoute] Guid userId)
    {
        try
        {
            var coreUser = await _userRepository.FindUserAsync(userId);

            return Ok(UserGetConverter.ConvertUserGetToDto(coreUser));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetUsersAsync));
            
            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete("{userId:guid}")]
    public async Task<IActionResult> DeleteUserAsync([FromRoute] Guid userId)
    {
        try
        {
            await _userRepository.DeleteUserAsync(userId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetUsersAsync));
            
            return StatusCode(500);
        }
    }
}