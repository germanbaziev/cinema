using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[ApiController]
[Route("/api/seat")]
public class SeatController : ControllerBase
{
    private readonly ISeatRepository _seatRepository;
    private readonly ILogger<SeatController> _logger;

    public SeatController(ISeatRepository seatRepository, ILogger<SeatController> logger)
    {
        _seatRepository = seatRepository;
        _logger = logger;
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutSeatAsync([FromBody] Seat seat)
    {
        try
        {
            if (!await _seatRepository.ExistAsync(seat.Id))
            {
                await _seatRepository.AddSeatAsync(seat.Id,
                    seat.Type,
                    seat.Row,
                    seat.Number,
                    seat.HallId);
            }
            else
            {
                await _seatRepository.UpdateSeatAsync(seat.Id,
                    seat.Type,
                    seat.Row,
                    seat.Number,
                    seat.HallId);
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutSeatAsync));

            return StatusCode(500);
        }
    }

    [HttpGet]
    public async Task<IActionResult> GetSeatsAsync()
    {
        try
        {
            var coreSeat = await _seatRepository.GetSeatsAsync();

            return Ok(coreSeat.Select(SeatConverter.ConvertSeatToDto).ToList());
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetSeatsAsync));

            return StatusCode(500);
        }
    }
    
    [HttpGet("{seatId:guid}")]
    public async Task<IActionResult> GetSeatAsync([FromRoute] Guid seatId)
    {
        try
        {
            var coreSeat = await _seatRepository.FindSeatAsync(seatId);

            if (coreSeat is null)
                return NotFound();

            return Ok(SeatConverter.ConvertSeatToDto(coreSeat));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetSeatAsync));

            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete("{seatId:guid}")]
    public async Task<IActionResult> DeleteSeatAsync([FromRoute] Guid seatId)
    {
        try
        {
            await _seatRepository.DeleteSeatAsync(seatId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(DeleteSeatAsync));

            return StatusCode(500);
        }
    }
}