using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Cinema.Core.Repositories;
using Cinema.Server.JwtAuth;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Cinema.Server.Controllers;

[ApiController]
[Route("/api/auth")]
public class AuthsController : ControllerBase
{
    private readonly IUserRepository _userRepository;
    private readonly IAuthRepository _authRepository;
    private readonly IConfiguration _configuration;
    private readonly ILogger<AuthsController> _logger;

    public AuthsController(IUserRepository userRepository, ILogger<AuthsController> logger, IConfiguration configuration, IAuthRepository authRepository)
    {
        _userRepository = userRepository;
        _logger = logger;
        _configuration = configuration;
        _authRepository = authRepository;
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> GetToken([FromBody] AuthData loginUser)
    {
        var user = await _userRepository.GetUserData(loginUser.Login, loginUser.Password);

        if (user is null)
            return Unauthorized("Неверный логин или пароль");
        
        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Role, user.Permissions)
        };

        var claim = new ClaimsIdentity(claims);
        var principal = new ClaimsPrincipal(claim);
        
        var tokenString = TokenGenerator.GenerateJwtToken(principal);
        
        return Ok(new { Token = tokenString, Message = "Success" });
    }
}