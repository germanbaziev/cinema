using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class TicketConverter
{
    public static Ticket? ConvertTicketToDto(Core.Models.Ticket? coreTicket)
    {
        if (coreTicket is null)
            return null;
        
        return new Ticket(coreTicket.Id,
            coreTicket.Price,
            coreTicket.Sold,
            coreTicket.UserId,
            coreTicket.SessionId,
            coreTicket.SeatId);
    }
    
    public static Core.Models.Ticket? ConvertTicketToCore(Ticket? dtoTicket)
    {
        if (dtoTicket is null)
            return null;
        
        return new Core.Models.Ticket(dtoTicket.Id,
            dtoTicket.Price,
            dtoTicket.Sold,
            dtoTicket.UserId,
            dtoTicket.SessionId,
            dtoTicket.SeatId);
    }
}