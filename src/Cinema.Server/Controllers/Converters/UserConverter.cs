using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class UserGetConverter
{
    public static UserGet? ConvertUserGetToDto(Core.Models.User? coreUser)
    {
        if (coreUser is null)
            return null;

        var coreTicket = coreUser.Tickets.Select(TicketConverter.ConvertTicketToDto).ToList();

        return new UserGet(coreUser.Id,
            coreUser.AvatarId,
            coreUser.Password,
            coreUser.Login,
            coreUser.Telegram,
            coreUser.Phone,
            coreUser.Email,
            coreUser.Permissions,
            coreTicket);
    }
    
    public static Core.Models.User? ConvertUserGetToCore(UserGet? dtoUser)
    {
        if (dtoUser is null)
            return null;

        var coreTicket = dtoUser.Tickets is null
            ? new List<Core.Models.Ticket>()
            : dtoUser.Tickets.Select(TicketConverter.ConvertTicketToCore).ToList()!;

        return new Core.Models.User(dtoUser.Id,
            dtoUser.AvatarId,
            dtoUser.Password,
            dtoUser.Login,
            dtoUser.Telegram,
            dtoUser.Phone,
            dtoUser.Email,
            dtoUser.Permissions,
            coreTicket);
    }
}