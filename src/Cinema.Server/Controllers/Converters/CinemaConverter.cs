using Cinema.Core.Models;

namespace Cinema.Server.Controllers.Converters;

public static class CinemaGetConverter
{
    public static Dto.Models.CinemaGet? ConvertCinemaGetToDto(Core.Models.Cinema? coreCinema)
    {
        if (coreCinema is null)
            return null;
        
        var dtoMovieCinemas = coreCinema.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToDto).ToList();

        var dtoHalls = coreCinema.Halls.Select(HallConverter.ConvertHallToDto).ToList();
        
        return new Dto.Models.CinemaGet(coreCinema.Id,
            coreCinema.Description,
            coreCinema.AvatarId,
            coreCinema.Address,
            coreCinema.Name,
            dtoMovieCinemas,
            dtoHalls);
    }
    
    public static Core.Models.Cinema? ConvertCinemaGetToCore(Dto.Models.CinemaGet? dtoCinemaGet)
    {
        if (dtoCinemaGet is null)
            return null;
        
        var dtoMovieCinemas = dtoCinemaGet.MovieCinemas is null
            ? new List<MovieCinema>()
            : dtoCinemaGet.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToCore).ToList()!;

        var dtoHalls = dtoCinemaGet.Halls is null
            ? new List<Hall>()
            : dtoCinemaGet.Halls.Select(HallConverter.ConvertHallToCore).ToList()!;
        
        return new Core.Models.Cinema(dtoCinemaGet.Id,
            dtoCinemaGet.Description,
            dtoCinemaGet.AvatarId,
            dtoCinemaGet.Address,
            dtoCinemaGet.Name,
            dtoMovieCinemas,
            dtoHalls);
    }
}