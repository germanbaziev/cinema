using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class MovieWorkerMovieConverter
{
    
    public static MovieWorkerMovie? ConvertMovieWorkerMovieToDto(Core.Models.MovieWorkerMovie? coreMovieWorkerMovie)
    {
        if (coreMovieWorkerMovie is null)
            return null;
        
        return new MovieWorkerMovie(coreMovieWorkerMovie.Id,
            coreMovieWorkerMovie.MovieWorkerId,
            coreMovieWorkerMovie.MovieId);
    }
    
    public static Core.Models.MovieWorkerMovie? ConvertMovieWorkerMovieToCore(MovieWorkerMovie? dtoMovieWorkerMovie)
    {
        if (dtoMovieWorkerMovie is null)
            return null;
        
        return new Core.Models.MovieWorkerMovie(dtoMovieWorkerMovie.Id,
            dtoMovieWorkerMovie.MovieWorkerId,
            dtoMovieWorkerMovie.MovieId);
    }
}