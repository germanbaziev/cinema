using Dto.Models;
using Seat = Cinema.Core.Models.Seat;

namespace Cinema.Server.Controllers.Converters;

public static class HallConverter
{
    public static Hall? ConvertHallToDto(Core.Models.Hall? coreHall)
    {
        if (coreHall is null)
            return null;

        var dtoSeat = coreHall.Seats.Select(SeatConverter.ConvertSeatToDto).ToList();

        return new Hall(coreHall.Id,
            coreHall.Number,
            coreHall.Type,
            coreHall.CinemaId,
            dtoSeat);
    }
    
    public static Core.Models.Hall? ConvertHallToCore(Hall? dtoHall)
    {
        if (dtoHall is null)
            return null;

        var coreSeats = dtoHall.Seats is null
            ? new List<Seat>()
            : dtoHall.Seats.Select(SeatConverter.ConvertSeatToCore).ToList()!;
        
        return new Core.Models.Hall(dtoHall.Id,
            dtoHall.Number,
            dtoHall.Type,
            dtoHall.CinemaId,
            coreSeats);
    }
}