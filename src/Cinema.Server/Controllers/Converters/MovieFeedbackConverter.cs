using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class MovieFeedbackConverter
{
    public static MovieFeedback? ConvertMovieFeedbackToDto(Core.Models.MovieFeedback? coreMovieFeedback)
    {
        if (coreMovieFeedback is null)
            return null;

        return new MovieFeedback(coreMovieFeedback.Id,
            coreMovieFeedback.Text,
            coreMovieFeedback.Date,
            coreMovieFeedback.Liked,
            coreMovieFeedback.MovieId,
            coreMovieFeedback.UserId);
    }
    
    public static Core.Models.MovieFeedback? ConvertMovieFeedbackToCore(MovieFeedback? dtoMovieFeedback)
    {
        if (dtoMovieFeedback is null)
            return null;

        return new Core.Models.MovieFeedback(dtoMovieFeedback.id,
            dtoMovieFeedback.Text,
            dtoMovieFeedback.Date,
            dtoMovieFeedback.Liked,
            dtoMovieFeedback.MovieId,
            dtoMovieFeedback.UserId);
    }
}