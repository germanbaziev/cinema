using Cinema.Core.Models;
using Dto.Models;
using MovieFeedback = Cinema.Core.Models.MovieFeedback;
using MovieWorkerMovie = Cinema.Core.Models.MovieWorkerMovie;
using Session = Cinema.Core.Models.Session;

namespace Cinema.Server.Controllers.Converters;

public static class MovieGetConverter
{
    public static MovieGet? ConvertMovieGetToDto(Core.Models.Movie? coreMovie)
    {
        if (coreMovie is null)
            return null;
        
        var dtoMovieWorkerMovie = coreMovie.MovieWorkerMovies.Select(MovieWorkerMovieConverter.ConvertMovieWorkerMovieToDto).ToList();

        var dtoMovieCinemas = coreMovie.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToDto).ToList();

        var dtoSessions = coreMovie.Sessions.Select(SessionConverter.ConvertSessionToDto).ToList();

        return new MovieGet(coreMovie.Id,
            coreMovie.TrailerId,
            coreMovie.AvatarId,
            coreMovie.Name,
            coreMovie.Description,
            coreMovie.Country,
            coreMovie.AgeLimit,
            coreMovie.Director,
            dtoMovieWorkerMovie,
            dtoMovieCinemas,
            dtoSessions);
    }
    
    public static Core.Models.Movie? ConvertMovieGetToCore(MovieGet? dtoMovieGet)
    {
        if (dtoMovieGet is null)
            return null;
        
        var coreMovieWorkerMovie = dtoMovieGet.MovieWorkerMovie is null
            ? new List<MovieWorkerMovie>()
            : dtoMovieGet.MovieWorkerMovie.Select(MovieWorkerMovieConverter.ConvertMovieWorkerMovieToCore).ToList()!;

        var coreMovieCinemas = dtoMovieGet.MovieCinemas is null
            ? new List<Core.Models.MovieCinema>()
            : dtoMovieGet.MovieCinemas.Select(MovieCinemaConverter.ConvertMovieCinemaToCore).ToList()!;

        var coreSessions = dtoMovieGet.Sessions is null
            ? new List<Session>()
            : dtoMovieGet.Sessions.Select(SessionConverter.ConvertSessionToCore).ToList()!;
        
        

        return new Core.Models.Movie(dtoMovieGet.Id,
            dtoMovieGet.TrailerId,
            dtoMovieGet.AvatarId,
            dtoMovieGet.Name,
            dtoMovieGet.Description,
            dtoMovieGet.Country,
            dtoMovieGet.AgeLimit,
            dtoMovieGet.Director,
            coreSessions,
            coreMovieWorkerMovie,
            new List<MovieFeedback>(),
            coreMovieCinemas);
    }
}