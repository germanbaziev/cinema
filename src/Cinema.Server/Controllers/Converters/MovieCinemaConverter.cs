using Dto.Models;
using Movie = Cinema.Core.Models.Movie;

namespace Cinema.Server.Controllers.Converters;

public static class MovieCinemaConverter
{
    public static MovieCinema? ConvertMovieCinemaToDto(Core.Models.MovieCinema? coreMovieCinema)
    {
        if (coreMovieCinema is null)
            return null;
        
        return new MovieCinema(coreMovieCinema.Id,
            coreMovieCinema.CinemaId,
            coreMovieCinema.MovieId);
    }
    
    public static Core.Models.MovieCinema? ConvertMovieCinemaToCore(MovieCinema? dtoMovieCinema)
    {
        if (dtoMovieCinema is null)
            return null;
        
        return new Core.Models.MovieCinema(dtoMovieCinema.Id,
            dtoMovieCinema.CinemaId,
            dtoMovieCinema.MovieId);
    }
}