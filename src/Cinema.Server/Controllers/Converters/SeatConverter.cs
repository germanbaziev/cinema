using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class SeatConverter
{
    public static Seat? ConvertSeatToDto(Core.Models.Seat? coreSeat)
    {
        if (coreSeat is null)
            return null;

        return new Seat(coreSeat.Id,
            coreSeat.Type,
            coreSeat.Row,
            coreSeat.Number,
            coreSeat.HallId);
    }
    
    public static Core.Models.Seat? ConvertSeatToCore(Seat? dtoSeat)
    {
        if (dtoSeat is null)
            return null;
        
        return new Core.Models.Seat(dtoSeat.Id,
            dtoSeat.Type,
            dtoSeat.Row,
            dtoSeat.Number,
            dtoSeat.HallId);
    }
}