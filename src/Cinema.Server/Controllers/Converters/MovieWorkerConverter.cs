using Dto.Models;

namespace Cinema.Server.Controllers.Converters;

public static class MovieWorkerConverter
{
    public static MovieWorker? ConvertMovieWorkerToDto(Core.Models.MovieWorker? coreMovieWorker)
    {
        if (coreMovieWorker is null)
            return null;
        
        var dtoMovieWorkerMovie = coreMovieWorker.MovieWorkerMovies.Select(coreMovieWorkerMovie => MovieWorkerMovieConverter.ConvertMovieWorkerMovieToDto(coreMovieWorkerMovie)).ToList();

        return new MovieWorker(coreMovieWorker.Id,
            coreMovieWorker.AvatarId,
            coreMovieWorker.Name,
            coreMovieWorker.Role,
            dtoMovieWorkerMovie);
    }
    
    public static Core.Models.MovieWorker? ConvertMovieWorkerToCore(MovieWorker? dtoMovieWorker)
    {
        if (dtoMovieWorker is null)
            return null;
        
        var coreMovieWorkerMovie = dtoMovieWorker.MovieWorkerMovies is null
            ? new List<Core.Models.MovieWorkerMovie>()
            : dtoMovieWorker.MovieWorkerMovies.Select(MovieWorkerMovieConverter.ConvertMovieWorkerMovieToCore).ToList()!;

        return new Core.Models.MovieWorker(dtoMovieWorker.Id,
            dtoMovieWorker.AvatarId,
            dtoMovieWorker.Name,
            dtoMovieWorker.Role,
            coreMovieWorkerMovie);
    }
}