using Dto.Models;
using Ticket = Cinema.Core.Models.Ticket;

namespace Cinema.Server.Controllers.Converters;

public static class SessionConverter
{
    public static Session? ConvertSessionToDto(Core.Models.Session? coreSession)
    {
        if (coreSession is null)
            return null;
        
        var dtoTicket = coreSession.Tickets.Select(TicketConverter.ConvertTicketToDto).ToList();
        
        return new Session(coreSession.Id,
            coreSession.StartDate,
            coreSession.MovieId,
            coreSession.HallId,
            coreSession.CinemaId,
            dtoTicket);
    }
    
    public static Core.Models.Session? ConvertSessionToCore(Session? dtoSession)
    {
        if (dtoSession is null)
            return null;
        
        var dtoTicket = dtoSession.Tickets is null
            ? new List<Ticket>()
            : dtoSession.Tickets.Select(TicketConverter.ConvertTicketToCore).ToList()!;
        
        return new Core.Models.Session(dtoSession.Id,
            dtoSession.StartDate,
            dtoSession.MovieId,
            dtoSession.HallId,
            dtoTicket,
            dtoSession.CinemaId);
    }
}