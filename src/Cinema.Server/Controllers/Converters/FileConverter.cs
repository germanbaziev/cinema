using File = Dto.Models.File;

namespace Cinema.Server.Controllers.Converters;

public static class FileConverter
{
    public static File? ConvertFileToDto(Core.Models.File? dtoFile)
    {
        if (dtoFile is null)
            return null;

        return new File(dtoFile.Id,
            dtoFile.Data,
            dtoFile.Name);
    }
    
    public static Core.Models.File? ConvertFileToCore(Dto.Models.File? coreFile)
    {
        if (coreFile is null)
            return null;

        return new Core.Models.File(coreFile.Id,
            coreFile.Data,
            coreFile.Name);
    }
}