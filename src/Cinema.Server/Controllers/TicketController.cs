using Cinema.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/ticket")]
public class TicketController : ControllerBase
{
    private readonly ITicketRepository _ticketRepository;
    private readonly ILogger<TicketController> _logger;

    public TicketController(ITicketRepository ticketRepository, ILogger<TicketController> logger)
    {
        _ticketRepository = ticketRepository;
        _logger = logger;
    }

    [HttpGet]
    public async Task<IActionResult> GetTicketsAsync()
    {
        try
        {
            var coreTickets = await _ticketRepository.GetTicketsAsync();

            return Ok(coreTickets.Select(TicketConverter.ConvertTicketToDto));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetTicketAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("{ticketId:guid}")]
    public async Task<IActionResult> GetTicketAsync([FromRoute] Guid ticketId)
    {
        try
        {
            var coreTicket = await _ticketRepository.FindTicketAsync(ticketId);

            if (coreTicket is null)
                return NotFound();

            return Ok(Converters.TicketConverter.ConvertTicketToDto(coreTicket));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetTicketAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutTicketAsync([FromBody] Ticket ticket)
    {
        try
        {
            if (!await _ticketRepository.ExistAsync(ticket.Id))
            {
                await _ticketRepository.AddTicketRepositories(ticket.Id,
                    ticket.Price,
                    ticket.Sold,
                    ticket.SessionId,
                    ticket.SeatId,
                    ticket.UserId);
            }
            else
            {
                await _ticketRepository.UpdateTicketAsync(ticket.Id,
                    ticket.Price,
                    ticket.Sold,
                    ticket.SessionId,
                    ticket.SeatId,
                    ticket.UserId);
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetTicketAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete("{TicketId:guid}")]
    public async Task<IActionResult> DeleteTicketAsync([FromRoute] Guid ticketId)
    {
        try
        {
            await _ticketRepository.DeleteTicketAsync(ticketId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetTicketAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("/seat/{seatId:guid}/sessionId/{sessionId:guid}")]
    public async Task<IActionResult> GetTicketBySeatAndSessionId([FromRoute] Guid seatId,
        [FromRoute] Guid sessionId)
    {
        try
        {
            var ticket = await _ticketRepository.FindTicketBySeatAndSessionIdAsync(seatId, sessionId);

            if (ticket is null)
                return NotFound();

            return Ok(TicketConverter.ConvertTicketToDto(ticket));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetTicketAsync));
            
            return StatusCode(500);
        }
    }
}