using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Dto.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/session")]
public class SessionController : ControllerBase
{
    private readonly ISessionRepository _sessionRepository;
    private readonly ILogger<SessionController> _logger;
    private readonly ITicketRepository _ticketRepository;

    public SessionController(ISessionRepository sessionRepository, ILogger<SessionController> logger, ITicketRepository ticketRepository)
    {
        _sessionRepository = sessionRepository;
        _logger = logger;
        _ticketRepository = ticketRepository;
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutSessionAsync([FromBody] Session session)
    {
        try
        {
            if (!await _sessionRepository.ExistAsync(session.Id))
            {
                await _sessionRepository.AddSessionAsync(session.Id,
                    session.StartDate,
                    session.MovieId,
                    session.HallId,
                    session.CinemaId);

                foreach (var ticket in session.Tickets)
                {
                    await _ticketRepository.AddTicketRepositories(ticket.Id,
                        ticket.Price,
                        false,
                        session.Id,
                        ticket.SeatId);
                }
            }
            else
            {
                await _sessionRepository.UpdateSessionAsync(session.Id,
                    session.StartDate,
                    session.MovieId,
                    session.HallId,
                    session.CinemaId);
                
                foreach (var ticket in session.Tickets)
                {
                    await _ticketRepository.UpdateTicketAsync(ticket.Id,
                        ticket.Price,
                        false,
                        session.Id,
                        ticket.SeatId);
                }
                
                
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutSessionAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("/cinema/{cinemaId:guid}/movie/{movieId:guid}")]
    public async Task<IActionResult> GetSessionsByCinemaAndMovieIdAsync([FromRoute] Guid cinemaId, [FromRoute] Guid movieId)
    {
        try
        {
            var coreSession = await _sessionRepository.GetSessionsByCinemaAndMovieIdAsync(cinemaId, movieId);

            return Ok(coreSession.Select(SessionConverter.ConvertSessionToDto));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutSessionAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete("{sessionId:guid}")]
    public async Task<IActionResult> DeleteSessionAsync([FromRoute] Guid sessionId)
    {
        try
        {
            await _sessionRepository.DeleteSessionAsync(sessionId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutSessionAsync));
            
            return StatusCode(500);
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpGet]
    public async Task<IActionResult> GetSessionsAsync()
    {
        try
        {
            var coreSessions = await _sessionRepository.GetSessionsAsync();

            return Ok(coreSessions.Select(SessionConverter.ConvertSessionToDto));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetSessionsAsync));
            
            return StatusCode(500);
        }
    }

}