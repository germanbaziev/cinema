using Cinema.Core.Repositories;
using Cinema.Server.Controllers.Converters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Server.Controllers;

[AllowAnonymous]
[ApiController]
[Route("/api/cinema")]
public class CinemaController : ControllerBase
{
    private readonly ICinemaRepository _cinemaRepositories;
    private readonly IHallRepository _hallRepository;
    private readonly IMovieCinemaRepository _movieCinemaRepository;
    private readonly ILogger<CinemaController> _logger;

    public CinemaController(ICinemaRepository cinemaRepositories,
        ILogger<CinemaController> logger,
        IMovieCinemaRepository movieCinemaRepository,
        IHallRepository hallRepository)
    {
        _cinemaRepositories = cinemaRepositories;
        _logger = logger;
        _movieCinemaRepository = movieCinemaRepository;
        _hallRepository = hallRepository;
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    public async Task<IActionResult> PutCinemaAsync(Dto.Models.CinemaPut cinema)
    {
        try
        {
            if(!await _cinemaRepositories.ExistAsync(cinema.Id))
            {
                await _cinemaRepositories.AddCinemaAsync(cinema.Id,
                    cinema.Description,
                    cinema.AvatarId,
                    cinema.Address,
                    cinema.Name);
                
                foreach (var movieCinema in cinema.MovieCinemas)
                {
                    await _movieCinemaRepository.AddMovieCinemaAsync(Guid.NewGuid(),
                        cinema.Id,
                        movieCinema.MovieId);
                }
            }
            else
            {
                await _cinemaRepositories.UpdateCinemaAsync(cinema.Id,
                    cinema.Description,
                    cinema.AvatarId,
                    cinema.Address,
                    cinema.Name);
                
                foreach (var movieCinema in cinema.MovieCinemas)
                {
                    await _movieCinemaRepository.AddMovieCinemaAsync(Guid.NewGuid(),
                        cinema.Id,
                        movieCinema.MovieId);
                }
            }

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(PutCinemaAsync));
            
            return StatusCode(500);
        }
    }
    
    [HttpGet]
    public async Task<IActionResult> GetCinemasAsync()
    {
        try
        {
            var coreCinemas = await _cinemaRepositories.GetCinemasAsync();

            return Ok(coreCinemas.Select(CinemaGetConverter.ConvertCinemaGetToDto));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetCinemaAsync));
            
            return StatusCode(500);
        }
    }

    [HttpGet("{cinemaId:guid}")]
    public async Task<IActionResult> GetCinemaAsync([FromRoute] Guid cinemaId)
    {
        try
        {
            var coreCinema = await _cinemaRepositories.FindCinemaAsync(cinemaId);

            if (coreCinema is null)
                return NotFound();
        
            return Ok(CinemaGetConverter.ConvertCinemaGetToDto(coreCinema));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetCinemaAsync));
            
            return StatusCode(500);
        }
    }
    
    [Authorize(Roles = "Admin")]
    [HttpDelete("{cinemaId:guid}")]
    public async Task<IActionResult> DeleteCinemaAsync([FromRoute] Guid cinemaId)
    {
        try
        {
            await _cinemaRepositories.DeleteCinemaAsync(cinemaId);

            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(DeleteCinemaAsync));
            
            return StatusCode(500);
        }
    }
    
    [HttpGet("{cinemaName}")]
    public async Task<IActionResult> GetCinemaByNameAsync([FromRoute] string cinemaName)
    {
        try
        {
            var coreCinema = await _cinemaRepositories.FindCinemaByNameAsync(cinemaName);

            if (coreCinema is null)
                return NotFound();
        
            return Ok(CinemaGetConverter.ConvertCinemaGetToDto(coreCinema));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error in method: {MethodName}", nameof(GetCinemaByNameAsync));
            
            return StatusCode(500);
        }
    }
}